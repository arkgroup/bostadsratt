<?php ?>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Bostadsrättsföreningen Bankgatan</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">
        <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/home.php" ? "active" : "");?>"><a href="home.php">Hem<span class="sr-only">(current)</span></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Föreningen <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/about.php" ? "active" : "");?>"><a href="about.php">Allmänt</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/history.php" ? "active" : "");?>"><a href="history.php">Historia</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/rules.php" ? "active" : "");?>"><a href="rules.php">Stadgar</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/economy.php" ? "active" : "");?>"><a href="economy.php">Ekonomi</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Att bo hos oss <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/secondhand.php" ? "active" : "");?>"><a href="secondhand.php">Andrahandsuthyrning</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/insurance.php" ? "active" : "");?>"><a href="insurance.php">Försäkring</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/fireprotection.php" ? "active" : "");?>"><a href="fireprotection.php">Brandskydd</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/radiator.php" ? "active" : "");?>"><a href="radiator.php">Element</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/electrical-safety.php" ? "active" : "");?>"><a href="electrical-safety.php">Elsäkerhet</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/fixardagar.php" ? "active" : "");?>"><a href="fixardagar.php">Fixardagar</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/journummer.php" ? "active" : "");?>"><a href="journummer.php">Journummer</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/ordningsregler.php" ? "active" : "");?>"><a href="ordningsregler.php">Ordningsregler</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/porttelefon.php" ? "active" : "");?>"><a href="porttelefon.php">Porttelefon</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/sophantering.php" ? "active" : "");?>"><a href="sophantering.php">Sophantering</a></li>       
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Gemensamma utrymmen <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/bastu.php" ? "active" : "");?>"><a href="bastu.php">Bastu</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/gillestuga.php" ? "active" : "");?>"><a href="gillestuga">Gillestuga</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/grillplats.php" ? "active" : "");?>"><a href="grillplats.php">Grillplats</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/odlingslotter.php" ? "active" : "");?>"><a href="odlingslotter.php">Odlingslotter</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/tvattstuga.php" ? "active" : "");?>"><a href="tvattstuga.php">Tvättstuga</a></li>           
          </ul>
        </li>
        <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/pictures.php" ? "active" : "");?>"><a href="pictures.php">Bilder</a></li>
        <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/blog.php" ? "active" : "");?>"><a href="blog.php">Nyheter</a></li>
        <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/contact.php" ? "active" : "");?>"><a href="contact.php">Kontakt</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Mina sidor <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/installningar.php" ? "active" : "");?>"><a href="installningar.php">Inställningar</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/booking.php" ? "active" : "");?>"><a href="booking.php">Tvättbokning</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/booking.php" ? "active" : "");?>"><a href="booking.php">Klagomål</a></li>
            <li class="<?php echo ($_SERVER['PHP_SELF'] == "/bostadsratt/booking.php" ? "active" : "");?>"><a href="booking.php">Logga ut</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>