<?php 
include('head.php');
include('navbar.php');
?>

<body>

	<div class="container">

		<div class="row">

			<div class="col-sm-6 col-sm-offset-3">

				<h1>Elsäkerhet</h1>
				<div class="separator"></div>

				<h4>Jordfelsbrytare och Automatsäkringar</h4>

				<p>
					Elskåpen i lägenheterna är utrustade med automatsäkringar och jordfelsbrytare. En automatsäkring ersätter den gamla typen av säkringar (proppar). När en propp löser ut skruvas den ur och ersätts med en ny. Detta är inte fallet med automatsäkringar, när de löser ut kan man slå till dem igen. På jordfelsbrytaren finns en provknapp som används för att kontrollera att jordfelsbrytaren är funktionsduglig. Kontrollen bör göras med högst 6 månaders intervall.
				</p>

				<h4>Felsökning</h4>
				<h5>
					Vad gör man när jordfelsbrytaren löser ut?
				</h5>

				<br>

				<p>
					<ol>
						<h5>Följande åtgärder rekommenderas</h5>
						<li>Slå till jordfelsbrytaren. Om jordfelsbrytaren inte löser ut igen tyder detta på en tillfällig störning, obefogad utlösning eller ett tillfälligt fel. Om detta förfarande uppträder igen några gånger bör hjälp sökas hos el-installatör.</li>
						<br>
						<li>Om jordfelsbrytaren löser ut igen direkt efter tillslaget finns ett bestående fel. Slå ifrån samtliga automatsäkringar i den aktuella centralen. Slå till jordfelsbrytaren igen. Återställ därefter en automatsäkring i taget tills jordfelsbrytaren löser ut igen. Felet är nu lokaliserat till den säkringsgrupp som återställdes sist. Om jordfelsbrytaren löser ut igen innan några säkringar har återställts finns felet i eller före centralen. Installatörens hjälp behövs.</li>
						<br>
						<li>Om felet är lokaliserat till en bestämd säkringsgrupp, fortsätt med att dra ur alla stickproppsanslutna apparater, inklusive lamputtagsanslutna, som hör till gruppen. Slå därefter till jordfelsbrytaren. Om den nu löser ut finns felet i den fasta installationen eller i någon fast ansluten apparat. Elinstallatörens hjälp behövs. Om jordfelsbrytaren förblir inkopplad, anslut en apparat i taget, till uttagen, tills jordfelsbrytaren löser ut igen. Det är den sist anslutna apparaten som sannolikt är felaktig.(Baserat på rekommendationer från Niléns Elektriska AB)</li>	
					</ol>
				</p>
			</div>

		</div>

		<hr>

	</div>

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>