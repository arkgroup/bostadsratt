<?php 
include('head.php');
include('navbar.php');
?>

<body>

	<!-- Container -->
	<div class="container">

		<div class="jumbotron">

			<div class="row">

				<h1>Kontakt</h1>
				<p>Fyll i formuläret nedan så svarar vi så fort vi kan!</p>

			</div>

			<form class="form-horizontal">

				<fieldset>

					<div class="row">

					<div class="form-group col-lg-4" id="input-1">

						<label class="control-label" for="firstname"></label>

						<input class="form-control" id="firstname" type="text" placeholder="Förnamn" required onclick="validInput('input-1')" oninvalid="invalidInput('input-1')">

					</div>

					</div>

					<div class="row">

					<div class="form-group col-lg-4" id="input-2">

						<label class="control-label" for="lastname"></label>

						<input class="form-control" id="lastname" type="text" placeholder="Efternamn" required oninput="validInput('input-2')" oninvalid="invalidInput('input-2')">

					</div>

					</div>

					<div class="row">

					<div class="form-group col-lg-4" id="input-3">

						<label class="control-label" for="email"></label>

						<input class="form-control" id="email" type="text" placeholder="Email" required onchange="validInput('input-3')" oninvalid="invalidInput('input-3')">

					</div>

					</div>

					<div class="row">

					<div class="form-group col-lg-4" id="input-4">

						<label class="control-label" for="phone"></label>

						<input class="form-control" id="phone" type="text" placeholder="Telefon" required onchange="validInput('input-4')" oninvalid="invalidInput('input-4')">

					</div>

					</div>

					<div class="row">

					<div class="form-group col-lg-4" id="input-5">
      					
      					<label for="textArea" class="control-label"></label>
        					
        				<textarea class="form-control" rows="3" id="textArea" style="margin-top: 0px; margin-bottom: 0px; height: 200px;" placeholder="Meddelande" required onchange="validInput('input-5')" oninvalid="invalidInput('input-5')"></textarea>
      						
    				</div>

					</div>

					<div class="row">

					<div class="form-group col-lg-4">

						<button type="submit" class="btn btn-primary">Skicka</button>

      				    <button type="reset" class="btn btn-default">Avbryt</button>

    				</div>

    				</div>

				</fieldset>

			</form>

		</div>

	</div>
	<!-- /Container -->

	<!-- Footer -->
	<?php 
	include('footer.php');
	?>

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Custom -->
	<script src="js/custom.js"></script>

</body>