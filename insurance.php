<?php 
include('head.php');
include('navbar.php');
?>

<body>

	<div class="container">

		<div class="row">

			<div class="col-sm-6 col-sm-offset-3">

				<h1>Försäkring</h1>
				<div class="separator"></div>

				<p>
					Föreningen har en fastighetsförsäkring som försäkrar själva huset, i den finns bl.a. en olycksfallsförsäkring för boende och besökare om det skulle ske något i föreningens gemensamma utrymmen eller i trädgården.
					<br><br>
					Vi har även möjlighet att få ohyra bekämpad med hjälp av Anticimex. Uppstår behov av detta så kontakta någon i styrelsen.
					<br><br>
					Det viktigaste när det gäller din egen försäkring är att du har ett så kallat bostadsrättstillägg på hemförsäkringen. Bor du på bottenvåningen bör du kolla med ditt försäkringsbolag om det gäller några särskilda regler för lås på fönstren.
					<br><br>
					Övriga detaljer angående försäkringen är knappast av sådant intresse att det måste finnas med i lägenhetspärmen. Har du frågor kontakta styrelsen.
				</p>

			</div>

		</div>

		<hr>

	</div>

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>