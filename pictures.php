<?php 
include('head.php');
include('navbar.php');
?>

<body>

    <!-- Page Content -->
    <div class="container">

        <div class="row text-center">

            <div class="col-sm-6 col-sm-offset-3">

            <h1>Bilder</h1>
            <div class="separator"></div>

            </div>

        </div>

        <div class="row">

                <div class="img-holder"><a href=""><img src="img/soldack.jpg" class="img-responsive col-sm-4 no-space portfolio-item"></a></div>
                <div class="img-holder"><a href=""><img src="img/uteplats.jpg" class="img-responsive col-sm-4 no-space portfolio-item"></a></div>
                <div class="img-holder"><a href=""><img src="img/uteplats-liten.jpg" class="img-responsive col-sm-4 no-space portfolio-item"></a></div>
                <div class="img-holder"><a href=""><img src="img/fasad.jpg" class="img-responsive col-sm-4 no-space portfolio-item"></a></div>
                <div class="img-holder"><a href=""><img src="img/bastu.jpg" class="img-responsive col-sm-4 no-space portfolio-item"></a></div>
                <div class="img-holder"><a href=""><img src="img/tvattstuga.jpg" class="img-responsive col-sm-4 no-space portfolio-item"></a></div>
                <div class="img-holder"><a href=""><img src="img/gillestuga.jpg" class="img-responsive col-sm-4 no-space portfolio-item"></a></div>
                <div class="img-holder"><a href=""><img src="img/balkong.jpg" class="img-responsive col-sm-4 no-space portfolio-item"></a></div>

        </div>

        <hr>

    </div>

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>