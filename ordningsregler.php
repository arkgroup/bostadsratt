<?php 
include('head.php');
include('navbar.php');
?>

<body>

	<div class="container">

		<div class="row">

			<div class="col-sm-6 col-sm-offset-3">

				<h1>Ordningsregler</h1>
				<div class="separator"></div>

				<ol>

					<li>Tvättider tecknas en åt gången. Ny tid tecknas först när föregående tid passerats. Tvätt- och torktider enligt schema i tvättstugan.</li>
					<br>
					<li>Efter avslutad tvättid ska tvättmaskinerna torkas av med fuktig trasa och tvättmedelsbehållarna torkas rena. Tvättluckorna ska lämnas öppna. Avloppsilen i diket bakom tvättmaskinerna ska plockas upp och rensas. Torktumlarens tre filter ska rengöras. Tvättstugan och torkrummet ska sopas och svabbas vid behov. Tänk på att avfuktaren går sönder om det är för dammigt i torkrummet. Lämna ej kvar tvättmedel i tvättstugan.</li>
					<br>
					<li>Soppåsar får aldrig ställas i trapphuset. Det är inte tillåtet att spika eller med häftstift fästa föremål på lägenhetsdörrens utsida. Vid transport av större föremål i trapphuset måste försiktighet iakttas.</li>
					<br>
					<li>Smutsar man ner i trappan städar man snarast upp efter sig.</li>
					<br>
					<li>Vinds- och källargångar måste hållas fria av brandtekniska skäl.</li>
					<br>
					<li>Gillestugan bokas hos ordförande i god tid. Man ska även sätta upp lappar på anslagstavlorna i bägge trapporna. Det kostar ingenting att låna gillestugan men vill man låna igen bör man städa noggrant efter sig.</li>
					<br>
					<li>Bastun bokas på lista utanför. Det är viktigt att bastun hålls ren p g a risk för bakteriespridning.</li>
					<br>
					<li>Grillplatserna bokas via lappar på anslagstavlorna i bägge trappor. Ange om det är den stora eller lilla grillplatsen som bokas. Grillarna tömmes efteråt.</li>
					<br>
					<li>Odlingslotterna tecknas för tre år i taget per lägenhet. Intresseanmälan lämnas till styrelsen. Innehavarna av odlingslotter har ett gemensamt ansvar för att vintertid hålla gården framkomlig efter snöfall. Detta gäller i synnerhet vid soptunnorna som annars riskerar att inte tömmas.</li>
					<br>
					<li>Alla sopor som slängs i soptunnorna ska vara väl paketerade. Bostadsrättshavaren transporterar själv bort grovsopor till kommunal sorteringsanläggning. Till kategorin grovsopor räknas t ex julgranar, möbler, hushållsmaskiner mm.</li>
					<br>
					<li>Tidningar och liknande pappersavfall slänts i papperscontainern. Kartonger ska rivas sönder och tidningar spridas jämnt i containern av utrymmesskäl. Vi betalar per tömning. Ingenting får ställas bredvid containern eller bredvid soptunnorna.</li>
					<br>
					<li>Cyklar får endast ställas i cykelställen. Aldrig i trapphus eller någon annanstans på gården. Undvik att ställa cykeln på trottoaren mot Bankgatan eller Tvärgatan. Tänk på de gamla och handikappade.</li>
					<br>
					<li>Passage genom häcken till andra gården är absolut förbjuden.</li>
					<br>
					<li>Anslagstavlan är endast till för husets medlemmar. Reklam tas omedelbart ner.</li>
					<br>
					<li>Husdjur skall hållas på ett sådant sätt att olägenheter för andra medlemmar aldrig uppstår.</li>
					<br>
					<li>Klockan 22.00 kväll före normal arbetsdag skall ljudnivån dämpas till en nivå som lämnar övriga medlemmar ostörda. Vid övriga tillfällen tillämpas sunt förnuft.</li>
					<br>
					<li>Släng inte cigarettfimpar och skräp på gården.</li>
					<br>
					<li>Två fixardagar anordnas om året med obligatorisk närvaro. Avslutningsvis två punkter som även är reglerade i bostadsrättslagen.</li>
					<br>
					<li>Efter stämmobeslut äger föreningen rätt att överlåta förrådsutrymmen, till vilka föreningen har nyttjanderätt, till enskild bostadsrättshavare. Vid ev lägenhetsutbyggnad på vind/källare skall insats erläggas samt speciellt avtal med föreningen upprättas. Utbyggnad får ej ske så att passage genom vind/källare omöjliggörs. Ett dylikt förvärv innebär en ökning av andelstalet i föreningen.</li>
					<br>
					<li>Ansökan om andrahandsuthyrning skall ske skriftligen hos styrelsen minst en månad innan andrahandshyresgästens inflyttning. Styrelsen kan endast ge tillstånd för högst ett år i taget. Detta tillstånd kan förenas med villkor. Permanent avflyttade eller dödsbon får inte hyra ut i andra hand.</li>
	
				</ol>

			</div>

		</div>

		<hr>

	</div>

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>	