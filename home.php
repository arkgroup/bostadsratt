<?php 
include('head.php');
include('navbar-logged-in.php');
?>

<body>

    <!-- Page Content -->
    <div class="container">

        <div class="separator"></div>

    <!-- Heading Row -->
    <header class="header">
        <div class="row">
            <div class="col-sm-8 same-height">
                <img class="img-responsive img-rounded" src="img/bostadsrattsforening.jpg" alt="">
            </div>
            <!-- /.col-md-8 -->
            <div class="col-sm-4 same-height">
                <h1>Välkommen till Brf Bankgatan!</h1>
                <p>
                    Hur vet man att en bostadsrättsförening är bra?
                    Enkelt. Man gör en checklista. Om föreningen har trevliga medlemmar, ett välskött hus, en ideellt arbetande styrelse med stor kontinuitet, en underbar trädgård med ett par grillplatser, ett soldäck, en gemensam festlokal med bastu i källaren, återkommande evenemang, folk av alla de slag, stora förråd, kryddträdgråd och pool - då är det en bra förening. 
                    Bostadsrättsföreningen har ingen pool, vi medger det. Men vi har allt det andra. Så du kan välja: var avundsjuk, eller slå till nästa gång en lägenhet blir ledig. 
                    Välkommen in!
                </p>
            <div class="bottom">
                <h4>Intresserad? Skicka ett mail!</h4>
                <a href="#"><i class="fa fa-envelope fa-5x"></i></a>
            </div>
            </div>
            <!-- /.col-md-4 -->
        </div>
    </header>
        <!-- /.row -->
    <hr>

    </div>

    <div class="container-fluid">
        <!-- Call to Action Well -->
        <div class="row">
            <a href="blog.php" id="well-btn">
            <div class="well text-center">
                <h3>Vad händer just nu på Bostadsrättsförening Bankgatan? Håll dig uppdaterad!</h3>
            </div>
            </a>
        </div>
        <!-- /.row -->
    </div>

    <div class="container">

        <div class="separator"></div>

    <section>

        <!-- Content Row -->
        <div class="row news-row">
            
            <article>
                   
                <a class="col-md-4 news news-first" href="blog.php">
                        
                    <header>

                        <h2>Årsmöte 17/4</h2>

                    </header>

                        <p>Just the good ol' boys, never meanin' no harm. Beats all you've ever saw, been in trouble with the law since the day they was born. Straight'nin' the curve, flat'nin' the hills. Someday the mountain might get 'em, but the law never will. Makin' their way, the only way they know how, that's just a little bit more than the law will allow. Just good ol' boys, wouldn't change if they could, fightin' the system like a true modern day Robin Hood. <b>Läs mer...</b></p>                       
                    
                </a>

            </article>
            <!-- /.col-md-4 -->

            <article>

                <a class="col-md-4 news news-second" href="blog.php">

                    <header>

                        <h2>Vårstädning 24/4</h2>

                    </header>

                        <p>I never spend much time in school but I taught ladies plenty. It's true I hire my body out for pay, hey hey. I've gotten burned over Cheryl Tiegs, blown up for Raquel Welch. But when I end up in the hay it's only hay, hey hey. I might jump an open drawbridge, or Tarzan from a vine. 'Cause I'm the unknown stuntman that makes Eastwood look so fine. <b>Läs mer...</b></p>
            
                </a>

            </article>
            <!-- /.col-md-4 -->

            <article>

                <a class="col-md-4 news news-third" href="blog.php">

                    <header>

                        <h2>Renovering 31/4</h2>

                    </header>

                        <p>Mutley, you snickering, floppy eared hound. When courage is needed, you're never around. Those medals you wear on your moth-eaten chest should be there for bungling at which you are best. So, stop that pigeon, stop that pigeon, stop that pigeon, stop that pigeon, stop that pigeon, stop that pigeon, stop that pigeon. Howwww! Nab him, jab him, tab him, grab him, stop that pigeon now. <b>Läs mer...</b></p>
                
                </a>

            </article>
            <!-- /.col-md-4 -->
            
        </div>
        <!-- /.row -->

    </section>

        <hr>

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>
