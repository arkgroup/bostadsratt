<?php ?>
<footer>
	<div class="container-fluid footer"> 
        <div class="row">  	
            <div id="map"></div>
        </div>
    </div>
    <div class="container">
        <div class="separator"></div>
        <div class="row text-center">
            <div class="col-sm-8 col-sm-offset-2">
                <a href="#"><i class="fa fa-facebook-official fa-5x"></i></a><div class="horizontal-separator"></div>
                <a href="#"><i class="fa fa-instagram fa-5x"></i></a><div class="horizontal-separator"></div>
                <a href="#"><i class="fa fa-twitter fa-5x"></i></a>
            </div>
        </div>
        <div class="separator"></div>
        <div class="row text-center">
            <div class="col-sm-8 col-sm-offset-2">
                <address>
                    Bostadsrättsförening Bankgatan<div class="horizontal-separator"></div>
                    Bankgatan 11B<div class="horizontal-separator"></div>
                    222 40, Lund<div class="horizontal-separator"></div>
                    Skåne, Sverige<div class="horizontal-separator"></div>
                    <abbr title="Telefon">Tel: </abbr>0761961588
                </address>
            </div>
        </div>
    </div>

    <!-- Google Maps -->
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

</footer>