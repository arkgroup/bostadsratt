<?php 
include('head.php');
include('navbar.php');
?>

<body>

	<div class="container">

		<div class="row">

			<div class="col-sm-6 col-sm-offset-3">

				<h1>Andrahandsuthyrning</h1>
				<div class="separator"></div>

				<h4>Regler för andrahandsupplåtelse i Brf Östbo</h4>

				<p>
					Andrahandsupplåtelse av lägenheter är i allmänhet inte i föreningens intresse, eftersom vi är en liten förening där trivseln i huset är beroende av att alla boende känner ett delat ansvar för husets och gårdens skick. Naturligtvis finns det ändå tillfällen då varje medlem kan ha behov av att under en period hyra eller låna ut sin lägenhet, och har sådana skäl för detta att tillstånd kommer att ges.
					<br><br>
					Styrelsen har ansvar för att godkänna såväl nya medlemmar i föreningen som andrahandshyresgäster. För att undvika tveksamhet om i vilka fall andrahandsupplåtelse kan ske finns nedanstående regler.
					<br><br>
					1. Andrahandsupplåtelse tillåtes under maximalt 2 år under förutsättning att avsikten är att flytta tillbaka efter den överenskomna tiden. Tillstånd ges för högst ett år i taget.
					<br><br>
					2. Godtagbara skäl för andrahandsupplåtelse är
					<ul>
						<li>studier eller arbete på ort utanför pendlingsavstånd</li>
						<li>sjukdom som nödvändiggör vistelse för vård i annan bostad</li>
						<li>värnplikt</li>
					</ul>
					3. Andrahandsupplåtelse skall alltid i förväg godkännas av styrelsen oavsett om upplåtelsen sker som uthyrning eller om lägenheten lånas ut.
					<br><br>
					4. Andrahandshyresgäst skall godkännas av styrelsen som skall ha kännedom om hyresgästens identitet samt ha rätt att begära referens från tidigare hyresvärd.
					<br><br>
					5. En kopia av kontrakt mellan lägenhetsinnehavaren och hyresgästen skall lämnas till föreningen. I kontraktet skall anges att hyresgästen har fått kännedom om de ordningsregler som finns och förbinder sig att följa dem, samt att kontraktet kan hävas på styrelsens begäran om så inte sker.
					<br><br>
					6. Styrelsen skall ha kännedom om lägenhetsinnehavarens adress under upplåtelsetiden.
					<br><br>		
					7. Om en andrahandshyresgäst uppträder störande för grannar eller avsiktligt orsakar skada på huset eller gården skall lägenhetsinnehavaren ansvara för att åtgärda dessa problem och på styrelsens begäran vid behov avbryta andrahandsupplåtelsen.
					<br><br>
					8. Om lägenhetsinnehavaren vid upprepade tidigare tillfällen upplåtit sin lägenhet till en gäst/hyresgäst som orsakat skador på huset eller störningar för grannar har styrelsen rätt att neka ny upplåtelse även om punkterna 1 och 2 kan anses tillgodosedda.
				</p>

			</div>

		</div>

		<hr>

	</div>

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>
