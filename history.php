	<?php 
	include('head.php');
	include('navbar.php');
	?>

	<body>

		<div class="container">

			<main>

				<div class="row vertical-align">

					<div class="col-sm-6 col-sm-offset-3">

						<section>

							<header>

								<h1>Historia</h1>

							</header>

				<div class="separator"></div>

							<article>

								<p>

									Nybygge på Ugglan 19
									1931 såg Lund annorlunda ut. Från bryggeriet på Kävlingevägen kördes ölen fortfarande ut med häst och vagn även om bilarna så smått hade börjat prägla stadsbilden. Männen bar nästan alltid huvudbonad och kvinnor bar aldrig byxor. På Östergatan hade ett gammalt ruckel från 1800-talet precis rivits och byggandet av ett nytt, modernt flerfamiljshus påbörjats. Byggherre var den nybildade bostadsrättsföreningen Östbo. 1932 stod huset klart och Östergatan hade då bytt namn till Bankgatan efter den bank som fortfarande ligger vid Mårtenstorget.
									Kostnaden för husbygget hölls på en låg nivå. Många av medlemmarna i den nybildade föreningen var hantverkare och kunde därför helt eller delvis arbeta med färdigställandet av det hus de själva skulle flytta in i med sina familjer. En av murarna hette Helge Nilsson. Han bodde i huset fram till 1994, vilket innebar att han kunde bidra med en del nyttiga tips under renoveringen 1993-94: ” Ta det lugnt när ni river ut badrummen i B-trappan, pågar. Vi råkade beställa för stora badkar när vi byggde huset, väggen mellan badkaren och grannens hall är knappt en halv tegelsten” . När Helge flyttade efter 62 år var han den sista av den första generationens medlemmar att lämna huset.
									Huset på tomten Ugglan 19 var ett arbetarhus även om de egentliga arbetarkvarteren låg i det angränsande och ganska fattiga bostadsområdet Nöden. Någon onödig utsmyckning av huset blev det inte, det enda tecknet på att det byggdes under Art Deco-eran är handtagen på ekdörrarna mot Bankgatan. Att fönstren mot gården har spröjsar och inte de mot gatan beror på att spröjsar inte ansågs fint. Större rutor var dyrare och därmed finare och därför lades dessa ut mot gatan. Av samma anledning valdes tegel av något billigare kvalitet mot gården. Skillnaden i teglet syns tydligt inne på gården där kortsidan av huset mot Stora Tvärgatan har betydligt mindre färgskiftningar än den övriga fasaden.
									Under andra världskriget byggdes källaren om för att kunna fungera som skyddsrum och det går fortfarande att upptäcka spår av detta för den som letar noga. Något som dock inte finns kvar längre är kokspannan och det stora lagret koks som utgjorde husets bränslereserv under kriget. Den stora högen av koks gjorde vi oss av med så sent som på nittiotalet.
									Fram till mitten av åttiotalet låg Max Magnussons tobaksaffär på hörnet Bankgatan - Stora Tvärgatan. När den lades ner gjorde Max om dörröppningen till fönster för att lägenheten skulle gå lättare att sälja. Skillnaden i putsbruket på utsidan syns ganska tydligt fortfarande.
								
								</p>

							</article>

						</section>

					</div>

				</div>

				<hr>

				<div class="row vertical-align">

					<div class="col-sm-6 col-sm-offset-3">

						<section>

							<header>

								<h1>Renoveringen</h1>

							</header>

				<div class="separator"></div>

							<article>

								<p>

									I slutet på åttio- och början på nittiotalet skedde en förändring i åldersstrukturen. De äldre lämnade plats för en yngre generation. Det första tecknet på detta var att trädgården gjordes om radikalt. Det enda som lämnades orört från den gamla innergården var två syrenträd. Ett av träden blåste ner i slutet på 90-talet, det andra står i skrivande stund fortfarande stadigt. Den tidigare gården var av klassikt 30-talssnitt och dominerades av en stor torkställning och en piskställning. Den täta häcken av avenbok som vi ser idag var, precis efter vi planterat den, en ganska klen samling träd. Eftersom det inte gick att se var vår trädgård slutade och grannens började byggde vi ett staket som idag är helt uppslukat av häcken.
									Mycket nöjda med vår nyanlagda trädgård och sittandes på våra nya uteplatser höjde vi blicken en aning och insåg då att resten av huset nog också behövde en omfattande översyn.
									I slutet av 1993, under en av mellandagarna faktiskt, togs de första spadtagen som inledde den kraftigaste renoveringen i husets historia. Egentligen togs inga spadtag – vi rev en vägg i källaren. Och anledningen till den sena tidpunkten på året var att vi ville komma i åtnjutande av de förmånliga räntebidragsregler som gällde för 1993.
									I juni 1994 skedde slutbesiktning av renoveringen. Då hade samtliga badrum renoverats och i samband med hade vi detta bytt både avlopps- och tappvattenledningar samt elsystemet i sin helhet. Detta innebar bl a att jordfelsbrytare och tappbrytare installerades i samtliga lägenheter, att elmätare för varje lägenhet placerades i en ny elcentral i källaren samt att alla uttag i huset jordades. I samband med detta installerades även kabel-TV.
									Den gamla oljeuppvärmningen ersattes med fjärrvärme. På så sätt frigjordes stora ytor i källaren och oljepannor, oljecisternen och en gigantisk vattentank fick lämna plats för bastu, pentry och gillestuga.
									Det enda som inte rördes under den stora renoveringen var elementen men de beräknas hålla en bra bit in på 2000-talet, bara de luftas någorlunda regelbundet.
									Ett nytt styrelserum inreddes i det gamla stenmangelrummet i källaren 1996. Det rummet har en egen utgång mot Bankgatan och lär ha inhyst ett skomakeri någon gång på trettiotalet. Tidigare låg styrelserummet längst upp i B-trappan, men när det rummet såldes och införlivades med en av vindslägenheterna, fick föreningsmaterial förvaras hos aktuell ordförande, kassör och sekreterare. Trots att därmed en hel del av föreningens material spreds ut, finns samtliga styrelseprotokoll, sedan föreningens bildande, bevarade.
									Östbo har alltid varit en privat bostadsrättsförening och har därför alltid varit helt beroende av att dess medlemmar har ställt upp på föreningen och för varandra. Att så har skett och att trivseln varit, och är, hög bevisas inte minst av det faktum att så många medlemmar genom åren valt att dels flytta inom huset, dels köpa angränsande lägenheter när familjerna växt.
									Lund juli 2006
									Anders Book Hallström
									Styrelsemedlem från och till mellan 1992-2006
								
								</p>

							</article>

						</section>

					</div>

				</div>

			</main>

			<hr>

		</div>

		<!-- Footer -->
		<?php 
		include('footer.php');
		?>

		<!-- jQuery -->
		<script src="js/jquery.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>

		<!-- Custom -->
		<script src="js/custom.js"></script>

	</body>