<?php 
include('head.php');
include('navbar-logged-in.php');
?>

<body>

    <div class="container">

        <div class="row">

            <div class="col-sm-6 col-sm-offset-3">

                <h1>Inställningar</h1>
                <div class="separator"></div>

                <div class="row">

                    <div class="col-sm-6">

                        <div class="list-group">
                            <a href="#" class="list-group-item">
                            <h4 class="list-group-item-heading">Profilbild</h4>
                            <p class="list-group-item-text">
                                Din profilbild används för att visa vem du är.
                                <br>
                                Det är inget krav att använda en profilbild.
                                <br><br>
                                Tryck här för att redigera din profilbild
                            </p>                                
                            </a>
                        </div>

                        <div class="list-group">
                            <a href="#" class="list-group-item">
                            <h4 class="list-group-item-heading">Lösenord</h4>
                            <p class="list-group-item-text">
                            Har du glömt bort ditt lösenord?
                            <br>
                            Vill du byta till ett lättare lösenord?
                            <br><br>
                            Tryck här för att redigera ditt lösenord
                            </p>                                
                            </a>
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="list-group">
                            <a href="#" class="list-group-item" data-toggle="modal" data-target="#color-modal">
                            <h4 class="list-group-item-heading">Färgtema</h4>
                            <p class="list-group-item-text">
                            Som användare kan du byta färgtema på 
                            <br>
                            hemsidan genom tre enkla knapptryck!
                            <br><br>
                            Tryck här för att byta hemsidans färgtema
                            </p>                                
                            </a>
                        </div>

                    </div>

                    <div class="modal fade" id="color-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="loginmodal-container">
      
                                <h1>Färgtema</h1><br>
                                
                                <div class="row">

                                    <div class="col-sm-6">

                                        <div class="panel color-light" id="light">

                                            <div class="panel-heading panel-light">

                                                <h3 class="panel-title text-center">Light</h3>

                                            </div>

                                            <div class="panel-body">

                                                <p class="chosen-color"></p>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-sm-6">

                                        <div class="panel color-dark" id="dark">

                                            <div class="panel-heading panel-light">

                                                <h3 class="panel-title text-center">Dark</h3>

                                            </div>

                                            <div class="panel-body dark">

                                                <p class="chosen-color"></p>

                                            </div>

                                        </div>

                                    </div>

                                    <a href="#" class="btn btn-save btn-lg btn-block">Spara</a>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <hr>

    </div>

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>
