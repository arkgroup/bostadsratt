function showMachines() {


	document.getElementById('machines').className = 'tab-pane fade active in';

}

function activeEconomy() {

  document.getElementById('info-1').className += ' active-info';
  document.getElementById('info-2').className = 'thumbnail';
  document.getElementById('info-3').className = 'thumbnail';
  document.getElementById('info-4').className = 'thumbnail';

}

function activeBoard() {

  document.getElementById('info-1').className = 'thumbnail';
  document.getElementById('info-2').className += ' active-info';
  document.getElementById('info-3').className = 'thumbnail';
  document.getElementById('info-4').className = 'thumbnail';

}

function activeBlueprint() {

  document.getElementById('info-1').className = 'thumbnail';
  document.getElementById('info-2').className = 'thumbnail';
  document.getElementById('info-3').className += ' active-info';
  document.getElementById('info-4').className = 'thumbnail';

}

function activeResident() {

  document.getElementById('info-1').className = 'thumbnail';
  document.getElementById('info-2').className = 'thumbnail';
  document.getElementById('info-3').className = 'thumbnail';
  document.getElementById('info-4').className += ' active-info';

}

function invalidInput(inv) {

  document.getElementById(inv).className += ' has-error';

}

function validInput(val) {

  document.getElementById(val).classname = 'form-group col-lg-4';

}

//hide months content and reset active tab
function resetMonth() {

	document.getElementById('jan-0').className = '';
	document.getElementById('feb-1').className = '';
	document.getElementById('mar-2').className = '';
	document.getElementById('apr-3').className = '';
	document.getElementById('may-4').className = '';
	document.getElementById('jun-5').className = '';
	document.getElementById('jul-6').className = '';
	document.getElementById('aug-7').className = '';
	document.getElementById('sep-8').className = '';
	document.getElementById('oct-9').className = '';
	document.getElementById('nov-10').className = '';
	document.getElementById('dec-11').className = '';
	document.getElementById('january').className = 'tab-pane fade hide';
	document.getElementById('february').className = 'tab-pane fade hide';
	document.getElementById('mars').className = 'tab-pane fade hide';
	document.getElementById('april').className = 'tab-pane fade hide';
  document.getElementById('may').className = 'tab-pane fade hide';
  document.getElementById('june').className = 'tab-pane fade hide';
  document.getElementById('july').className = 'tab-pane fade hide';
  document.getElementById('august').className = 'tab-pane fade hide';
  document.getElementById('september').className = 'tab-pane fade hide';
  document.getElementById('october').className = 'tab-pane fade hide';
  document.getElementById('november').className = 'tab-pane fade hide';
  document.getElementById('december').className = 'tab-pane fade hide';

}

//make current month and day active
function thisMonth() {

	var cal_months_labels = ['january', 'february', 'mars', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];
    var dayMonth = new Date();
	var day = dayMonth.getDate();
	var month = dayMonth.getMonth();
	var today = cal_months_labels[month] + '-' + day;

	document.getElementById('january').className = 'tab-pane fade';
	document.getElementById('february').className = 'tab-pane fade';
	document.getElementById('mars').className = 'tab-pane fade';
	document.getElementById('april').className = 'tab-pane fade';
    document.getElementById('may').className = 'tab-pane fade';
    document.getElementById('june').className = 'tab-pane fade';
    document.getElementById('july').className = 'tab-pane fade';
    document.getElementById('august').className = 'tab-pane fade';
    document.getElementById('september').className = 'tab-pane fade';
    document.getElementById('october').className = 'tab-pane fade';
    document.getElementById('november').className = 'tab-pane fade';
    document.getElementById('december').className = 'tab-pane fade';	

	switch (month) {

    	case 0:
    	    document.getElementById('jan-0').className = 'active';
    	    document.getElementById('january').className = 'tab-pane fade active in';
    	    document.getElementById(today).className += ' active-day';
    	    break; 
   		 case 1:
     	    document.getElementById('feb-1').className = 'active';
     	    document.getElementById('february').className = 'tab-pane fade active in';
     	    document.getElementById(today).className += ' active-day';
      	    break;
      	case 2:
     	    document.getElementById('mar-2').className = 'active';
     	    document.getElementById('mars').className = 'tab-pane fade active in';
     	    document.getElementById(today).className += ' active-day';
      	    break;
      	case 3:
     	    document.getElementById('apr-3').className = 'active';
     	    document.getElementById('april').className = 'tab-pane fade active in';
     	    document.getElementById(today).className += ' active-day';
      	    break;
      	case 4:
     	    document.getElementById('may-4').className = 'active';
     	    document.getElementById('may').className = 'tab-pane fade active in';
     	    document.getElementById(today).className += ' active-day';
      	    break;
      	case 5:
     	    document.getElementById('jun-5').className = 'active';
     	    document.getElementById('june').className = 'tab-pane fade active in';
     	    document.getElementById(today).className += ' active-day';
      	    break;
      	case 6:
     	    document.getElementById('jul-6').className = 'active';
     	    document.getElementById('july').className = 'tab-pane fade active in';
     	    document.getElementById(today).className += ' active-day';
      	    break;
      	case 7:
     	    document.getElementById('aug-7').className = 'active';
     	    document.getElementById('august').className = 'tab-pane fade active in';
     	    document.getElementById(today).className += ' active-day';
      	    break;
      	case 8:
     	    document.getElementById('sep-8').className = 'active';
     	    document.getElementById('september').className = 'tab-pane fade active in';
     	    document.getElementById(today).className += ' active-day';
      	    break;
      	case 9:
     	    document.getElementById('oct-9').className = 'active';
     	    document.getElementById('october').className = 'tab-pane fade active in';
     	    document.getElementById(today).className += ' active-day';
      	    break;
      	case 10:
     	    document.getElementById('nov-10').className = 'active';
     	    document.getElementById('november').className = 'tab-pane fade active in';
     	    document.getElementById(today).className += ' active-day';
      	    break;
      	case 11:
     	    document.getElementById('dec-11').className = 'active';
     	    document.getElementById('december').className = 'tab-pane fade active in';
     	    document.getElementById(today).className += ' active-day';
      	    break;
    	default: 
        	break;
	}

}

function colorToHex(color) {
    if (color.substr(0, 1) === '#') {
        return color;
    }
    var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color);
    var red = parseInt(digits[2]);
    var green = parseInt(digits[3]);
    var blue = parseInt(digits[4]);
    var rgb = blue | (green << 8) | (red << 16);
    return digits[1] + '#' + rgb.toString(16);
};

function changeColor(from, to) {
   var elements = document.getElementsByTagName('*');
   for (var i=0;i<elements.length;i++) {
      var color = window.getComputedStyle(elements[i]).color;
      var hex = colorToHex(color);
      if (hex == from) {
         elements[i].style.color=to;
      }
      var backgroundColor = window.getComputedStyle(elements[i]).backgroundColor;
      if (backgroundColor.indexOf('rgba')<0) {
          var hex = colorToHex(backgroundColor);
          if (hex == from) {
             elements[i].style.backgroundColor=to;
          }
      }

   }
}   

// change all red color styled elements to blue
document.getElementById('dark').onclick = function() {
   changeColor('#ffffff', '#a5adb0');
   changeColor('#2196f3', '#ff9800');
   changeColor('#9c27b0', '#ff9800');
   changeColor('#0c7cd5', '#e68900');
   changeColor('#666666', '#f3f3f3');
   changeColor('#444444', '#f3f3f3');
   changeColor('#bbbbbb', '#f3f3f3');
   changeColor('#333333', '#f3f3f3');
}

document.getElementById('light').onclick = function() {
   changeColor('#a5adb0', '#ffffff');
   changeColor('#ff9800', '#2196f3');
   changeColor('#f3f3f3', '#333333');
}


    // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,
                     scrollwheel: false,
                      navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    draggable: false,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(55.699344, 13.195766), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(55.699344, 13.195766),
                    map: map,
                    title: 'Bankgatan 11B'
                });
            }