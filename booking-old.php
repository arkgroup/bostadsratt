<?php 
include('head.php');
?>

<body onload="thisMonth(), thisDay()">

<!-- Page Content -->
    <div class="container">

        <!-- Jumbotron Header -->
        <header class="jumbotron hero-spacer">
            <h1>Tvättbokning</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, ipsam, eligendi, in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>
            <p><a class="btn btn-primary btn-large">Call to action!</a>
            </p>
        </header>

        <hr>

        <!-- Title -->
        <div class="booking-table">
        <ul class="nav nav-tabs center">
  			<li class="active">
  				<a href="#available" data-toggle="tab" aria-expanded="true" onclick="document.getElementById('days').className = 'tab-content', thisMonth()">
  					<div class="row">
  						<div class="col-lg-12">
                			<h3>Tillgängliga tider</h3>
           				</div>
        			</div>
    			</a>
    		</li>
    		<li class="">
  				<a href="#reserved" data-toggle="tab" aria-expanded="true" onclick="document.getElementById('days').className += ' hide', resetMonth()">
  					<div class="row">
  						<div class="col-lg-12">
                			<h3>Dina bokade tider</h3>
           				</div>
        			</div>
    			</a>
    		</li>
    	</ul>
       
        <!-- /.row -->

        <!-- Page Content -->
        <div class="row">
<div class="col-sm-12">

	<!-- Months -->

<div id="months" class="tab-content">

		<!-- Available -->

  <div class="tab-pane fade active in" id="available">
    <ul class="nav nav-tabs">
  		<li class="" id="jan-0"><a href="#january" data-toggle="tab" aria-expanded="true">Januari</a></li>
  		<li class="" id="feb-1"><a href="#february" data-toggle="tab" aria-expanded="false">Februari</a></li>
  		<li class="" id="mar-2"><a href="#mars" data-toggle="tab" aria-expanded="false">Mars</a></li>
 		<li class="" id="apr-3"><a href="#april" data-toggle="tab" aria-expanded="false">April</a></li>
  		<li class="" id="may-4"><a href="#may" data-toggle="tab" aria-expanded="false">Maj</a></li>
  		<li class="" id="jun-5"><a href="#june" data-toggle="tab" aria-expanded="false">Juni</a></li>
  		<li class="" id="jul-6"><a href="#july" data-toggle="tab" aria-expanded="false">Juli</a></li>
  		<li class="" id="aug-7"><a href="#august" data-toggle="tab" aria-expanded="false">Augusti</a></li>
  		<li class="" id="sep-8"><a href="#september" data-toggle="tab" aria-expanded="false">September</a></li>
  		<li class="" id="oct-9"><a href="#october" data-toggle="tab" aria-expanded="false">Oktober</a></li>
  		<li class="" id="nov-10"><a href="#november" data-toggle="tab" aria-expanded="false">November</a></li>
  		<li class="" id="dec-11"><a href="#december" data-toggle="tab" aria-expanded="false">December</a></li>
	</ul>
  </div>

  		<!-- Reserved -->

  <div class="tab-pane fade" id="reserved">
    <h1>Inga bokade tider</h1>
  </div>
</div>

	<!-- /Months -->

	<!-- Days -->

<div id="days" class="tab-content">

		<!-- January -->

	<div class="tab-pane fade" id="january">
		<table class="table table-striped table-hover">
  			<thead>
    			<tr>
	   		 	  <th class="col-md-1">MÅNDAG</th>
    			  <th class="col-md-1">TISDAG</th>
    			  <th class="col-md-1">ONSDAG</th>
    			  <th class="col-md-1">TORSDAG</th>
    			  <th class="col-md-1">FREDAG</th>
    			  <th class="col-md-1">LÖRDAG</th>
    			  <th class="col-md-1">SÖNDAG</th>
   				</tr>
  			</thead>
 			<tbody>
   				<tr>
      		      <td class="col-md-1 day" id="day-1"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">1</a></td>
      			  <td class="col-md-1 day" id="day-2"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">2</a></td>
      			  <td class="col-md-1 day" id="day-3"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">3</a></td>
      			  <td class="col-md-1 day" id="day-4"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">4</a></td>
      			  <td class="col-md-1 day" id="day-5"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">5</a></td>
      			  <td class="col-md-1 day" id="day-6"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">6</a></td>
      			  <td class="col-md-1 day" id="day-7"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">7</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day" id="day-8"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">8</a></td>
      			  <td class="col-md-1 day" id="day-9"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">9</a></td>
      			  <td class="col-md-1 day" id="day-10"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">10</a></td>
      			  <td class="col-md-1 day" id="day-11"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">11</a></td>
      			  <td class="col-md-1 day" id="day-12"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">12</a></td>
      			  <td class="col-md-1 day" id="day-13"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">13</a></td>
      			  <td class="col-md-1 day" id="day-14"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">14</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day" id="day-15"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">15</a></td>
      			  <td class="col-md-1 day" id="day-16"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">16</a></td>
      			  <td class="col-md-1 day" id="day-17"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">17</a></td>
      			  <td class="col-md-1 day" id="day-18"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">18</a></td>
      			  <td class="col-md-1 day" id="day-19"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">19</a></td>
      			  <td class="col-md-1 day" id="day-20"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">20</a></td>
      			  <td class="col-md-1 day" id="day-21"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">21</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day" id="day-22"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">22</a></td>
      			  <td class="col-md-1 day" id="day-23"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">23</a></td>
      			  <td class="col-md-1 day" id="day-24"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">24</a></td>
      			  <td class="col-md-1 day" id="day-25"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">25</a></td>
      			  <td class="col-md-1 day" id="day-26"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">26</a></td>
      			  <td class="col-md-1 day" id="day-27"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">27</a></td>
      			  <td class="col-md-1 day" id="day-28"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">28</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day" id="day-29"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">29</a></td>
      			  <td class="col-md-1 day" id="day-30"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">30</a></td>
      			  <td class="col-md-1 day" id="day-31"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">31</a></td>
      			  <td class="col-md-1">1</td>
      			  <td class="col-md-1">2</td>
      			  <td class="col-md-1">3</td>
      			  <td class="col-md-1">4</td>
    			</tr>
    		</tbody>
		</table> 

				<!-- Machines -->

		<div class="tab-pane fade hide" id="machines">
    		<div class="list-group">
    				<h3>Tillgängliga tvättmaskiner</h3>
    				<h4 id="today">Måndag 1 January, 2016</h4>
    			<div class="machine-wrapper col-sm-12 list-group-item">
    				<h4 class="list-group-item-heading">8.00-12.00 - Tvättmaskin 1
    				<a href="#" class="btn btn-success btn-lg book-btn">Boka</a></h4>
    			</div>
    			<div class="machine-wrapper col-sm-12 list-group-item">
    				<h4 class="list-group-item-heading">8.00-12.00 - Tvättmaskin 2
    				<a href="#" class="btn btn-success btn-lg book-btn">Boka</a></h4>
    			</div>
    			<div class="machine-wrapper col-sm-12 list-group-item">
    				<h4 class="list-group-item-heading">8.00-12.00 - Tvättmaskin 3
    				<a href="#" class="btn btn-success btn-lg book-btn">Boka</a></h4>
    			</div>
    			<div class="machine-wrapper col-sm-12 list-group-item">
    				<h4 class="list-group-item-heading">12.00-16.00 - Tvättmaskin 1
    				<a href="#" class="btn btn-success btn-lg book-btn">Boka</a></h4>
    			</div>
    			<div class="machine-wrapper col-sm-12 list-group-item">
    				<h4 class="list-group-item-heading">12.00-16.00 - Tvättmaskin 2
    				<a href="#" class="btn btn-success btn-lg book-btn">Boka</a></h4>
    			</div>
    			<div class="machine-wrapper col-sm-12 list-group-item">
    				<h4 class="list-group-item-heading">12.00-16.00 - Tvättmaskin 3
    				<a href="#" class="btn btn-success btn-lg book-btn">Boka</a></h4>
    			</div>
    			<div class="machine-wrapper col-sm-12 list-group-item unavailable">
    				<h4 class="list-group-item-heading">16.00-20.00 - Tvättmaskin 1
    				<a href="#" class="btn btn-danger btn-lg book-btn disabled">Boka</a></h4>
    			</div>
    			<div class="machine-wrapper col-sm-12 list-group-item unavailable">
    				<h4 class="list-group-item-heading">16.00-20.00 - Tvättmaskin 2
    				<a href="#" class="btn btn-danger btn-lg book-btn disabled">Boka</a></h4>
    			</div>
    			<div class="machine-wrapper col-sm-12 list-group-item unavailable">
    				<h4 class="list-group-item-heading">16.00-20.00 - Tvättmaskin 3
    				<a href="#" class="btn btn-danger btn-lg book-btn disabled">Boka</a></h4>
    			</div>
    		</div>
  		</div>
	</div>

		<!-- February -->

	<div class="tab-pane fade" id="february">
		<table class="table table-striped table-hover">
  			<thead>
    			<tr>
	   		 	  <th class="col-md-1">MÅNDAG</th>
    			  <th class="col-md-1">TISDAG</th>
    			  <th class="col-md-1">ONSDAG</th>
    			  <th class="col-md-1">TORSDAG</th>
    			  <th class="col-md-1">FREDAG</th>
    			  <th class="col-md-1">LÖRDAG</th>
    			  <th class="col-md-1">SÖNDAG</th>
   				</tr>
  			</thead>
 			<tbody>
   				<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">1</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">2</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">3</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">4</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">5</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">6</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">7</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">8</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">9</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">10</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">11</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">12</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">13</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">14</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">15</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">16</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">17</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">18</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">19</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">20</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">21</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">22</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">23</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">24</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">25</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">26</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">27</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">28</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">29</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">30</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false">31</a></td>
      			  <td class="col-md-1">1</td>
      			  <td class="col-md-1">2</td>
      			  <td class="col-md-1">3</td>
      			  <td class="col-md-1">4</td>
    			</tr>
    		</tbody>
		</table> 
	</div>

		<!-- Mars -->

	<div class="tab-pane fade" id="mars">
		<table class="table table-striped table-hover">
  			<thead>
    			<tr>
	   		 	  <th class="col-md-1">MÅNDAG</th>
    			  <th class="col-md-1">TISDAG</th>
    			  <th class="col-md-1">ONSDAG</th>
    			  <th class="col-md-1">TORSDAG</th>
    			  <th class="col-md-1">FREDAG</th>
    			  <th class="col-md-1">LÖRDAG</th>
    			  <th class="col-md-1">SÖNDAG</th>
   				</tr>
  			</thead>
 			<tbody>
   				<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">1</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">2</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">3</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">4</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">5</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">6</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">7</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">8</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">9</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">10</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">11</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">12</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">13</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">14</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">15</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">16</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">17</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">18</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">19</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">20</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">21</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">22</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">23</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">24</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">25</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">26</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">27</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">28</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">29</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">30</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">31</a></td>
      			  <td class="col-md-1">1</td>
      			  <td class="col-md-1">2</td>
      			  <td class="col-md-1">3</td>
      			  <td class="col-md-1">4</td>
    			</tr>
    		</tbody>
		</table> 
	</div>

		<!-- April -->

	<div class="tab-pane fade" id="april">
		<table class="table table-striped table-hover">
  			<thead>
    			<tr>
	   		 	  <th class="col-md-1">MÅNDAG</th>
    			  <th class="col-md-1">TISDAG</th>
    			  <th class="col-md-1">ONSDAG</th>
    			  <th class="col-md-1">TORSDAG</th>
    			  <th class="col-md-1">FREDAG</th>
    			  <th class="col-md-1">LÖRDAG</th>
    			  <th class="col-md-1">SÖNDAG</th>
   				</tr>
  			</thead>
 			<tbody>
   				<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">1</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">2</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">3</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">4</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">5</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">6</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">7</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">8</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">9</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">10</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">11</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">12</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">13</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">14</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">15</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">16</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">17</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">18</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">19</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">20</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">21</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">22</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">23</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">24</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">25</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">26</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">27</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">28</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">29</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">30</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">31</a></td>
      			  <td class="col-md-1">1</td>
      			  <td class="col-md-1">2</td>
      			  <td class="col-md-1">3</td>
      			  <td class="col-md-1">4</td>
    			</tr>
    		</tbody>
		</table> 
	</div>

		<!-- May -->

	<div class="tab-pane fade" id="may">
		<table class="table table-striped table-hover">
  			<thead>
    			<tr>
	   		 	  <th class="col-md-1">MÅNDAG</th>
    			  <th class="col-md-1">TISDAG</th>
    			  <th class="col-md-1">ONSDAG</th>
    			  <th class="col-md-1">TORSDAG</th>
    			  <th class="col-md-1">FREDAG</th>
    			  <th class="col-md-1">LÖRDAG</th>
    			  <th class="col-md-1">SÖNDAG</th>
   				</tr>
  			</thead>
 			<tbody>
   				<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">1</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">2</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">3</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">4</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">5</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">6</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">7</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">8</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">9</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">10</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">11</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">12</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">13</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">14</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">15</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">16</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">17</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">18</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">19</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">20</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">21</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">22</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">23</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">24</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">25</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">26</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">27</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">28</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">29</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">30</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">31</a></td>
      			  <td class="col-md-1">1</td>
      			  <td class="col-md-1">2</td>
      			  <td class="col-md-1">3</td>
      			  <td class="col-md-1">4</td>
    			</tr>
    		</tbody>
		</table> 
	</div>

		<!-- June -->

	<div class="tab-pane fade" id="june">
		<table class="table table-striped table-hover">
  			<thead>
    			<tr>
	   		 	  <th class="col-md-1">MÅNDAG</th>
    			  <th class="col-md-1">TISDAG</th>
    			  <th class="col-md-1">ONSDAG</th>
    			  <th class="col-md-1">TORSDAG</th>
    			  <th class="col-md-1">FREDAG</th>
    			  <th class="col-md-1">LÖRDAG</th>
    			  <th class="col-md-1">SÖNDAG</th>
   				</tr>
  			</thead>
 			<tbody>
   				<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">1</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">2</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">3</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">4</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">5</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">6</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">7</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">8</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">9</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">10</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">11</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">12</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">13</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">14</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">15</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">16</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">17</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">18</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">19</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">20</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">21</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">22</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">23</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">24</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">25</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">26</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">27</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">28</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">29</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">30</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">31</a></td>
      			  <td class="col-md-1">1</td>
      			  <td class="col-md-1">2</td>
      			  <td class="col-md-1">3</td>
      			  <td class="col-md-1">4</td>
    			</tr>
    		</tbody>
		</table> 
	</div>

		<!-- July -->

	<div class="tab-pane fade" id="july">
		<table class="table table-striped table-hover">
  			<thead>
    			<tr>
	   		 	  <th class="col-md-1">MÅNDAG</th>
    			  <th class="col-md-1">TISDAG</th>
    			  <th class="col-md-1">ONSDAG</th>
    			  <th class="col-md-1">TORSDAG</th>
    			  <th class="col-md-1">FREDAG</th>
    			  <th class="col-md-1">LÖRDAG</th>
    			  <th class="col-md-1">SÖNDAG</th>
   				</tr>
  			</thead>
 			<tbody>
   				<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">1</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">2</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">3</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">4</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">5</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">6</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">7</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">8</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">9</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">10</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">11</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">12</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">13</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">14</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">15</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">16</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">17</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">18</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">19</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">20</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">21</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">22</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">23</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">24</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">25</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">26</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">27</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">28</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">29</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">30</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">31</a></td>
      			  <td class="col-md-1">1</td>
      			  <td class="col-md-1">2</td>
      			  <td class="col-md-1">3</td>
      			  <td class="col-md-1">4</td>
    			</tr>
    		</tbody>
		</table> 
	</div>

		<!-- August -->

	<div class="tab-pane fade" id="august">
		<table class="table table-striped table-hover">
  			<thead>
    			<tr>
	   		 	  <th class="col-md-1">MÅNDAG</th>
    			  <th class="col-md-1">TISDAG</th>
    			  <th class="col-md-1">ONSDAG</th>
    			  <th class="col-md-1">TORSDAG</th>
    			  <th class="col-md-1">FREDAG</th>
    			  <th class="col-md-1">LÖRDAG</th>
    			  <th class="col-md-1">SÖNDAG</th>
   				</tr>
  			</thead>
 			<tbody>
   				<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">1</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">2</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">3</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">4</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">5</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">6</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">7</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">8</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">9</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">10</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">11</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">12</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">13</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">14</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">15</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">16</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">17</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">18</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">19</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">20</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">21</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">22</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">23</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">24</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">25</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">26</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">27</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">28</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">29</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">30</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">31</a></td>
      			  <td class="col-md-1">1</td>
      			  <td class="col-md-1">2</td>
      			  <td class="col-md-1">3</td>
      			  <td class="col-md-1">4</td>
    			</tr>
    		</tbody>
		</table> 
	</div>

		<!-- September -->

	<div class="tab-pane fade" id="september">
		<table class="table table-striped table-hover">
  			<thead>
    			<tr>
	   		 	  <th class="col-md-1">MÅNDAG</th>
    			  <th class="col-md-1">TISDAG</th>
    			  <th class="col-md-1">ONSDAG</th>
    			  <th class="col-md-1">TORSDAG</th>
    			  <th class="col-md-1">FREDAG</th>
    			  <th class="col-md-1">LÖRDAG</th>
    			  <th class="col-md-1">SÖNDAG</th>
   				</tr>
  			</thead>
 			<tbody>
   				<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">1</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">2</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">3</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">4</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">5</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">6</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">7</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">8</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">9</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">10</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">11</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">12</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">13</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">14</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">15</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">16</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">17</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">18</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">19</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">20</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">21</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">22</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">23</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">24</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">25</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">26</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">27</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">28</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">29</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">30</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">31</a></td>
      			  <td class="col-md-1">1</td>
      			  <td class="col-md-1">2</td>
      			  <td class="col-md-1">3</td>
      			  <td class="col-md-1">4</td>
    			</tr>
    		</tbody>
		</table> 
	</div>

		<!-- October -->

	<div class="tab-pane fade" id="october">
		<table class="table table-striped table-hover">
  			<thead>
    			<tr>
	   		 	  <th class="col-md-1">MÅNDAG</th>
    			  <th class="col-md-1">TISDAG</th>
    			  <th class="col-md-1">ONSDAG</th>
    			  <th class="col-md-1">TORSDAG</th>
    			  <th class="col-md-1">FREDAG</th>
    			  <th class="col-md-1">LÖRDAG</th>
    			  <th class="col-md-1">SÖNDAG</th>
   				</tr>
  			</thead>
 			<tbody>
   				<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">1</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">2</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">3</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">4</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">5</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">6</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">7</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">8</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">9</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">10</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">11</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">12</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">13</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">14</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">15</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">16</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">17</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">18</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">19</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">20</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">21</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">22</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">23</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">24</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">25</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">26</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">27</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">28</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">29</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">30</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">31</a></td>
      			  <td class="col-md-1">1</td>
      			  <td class="col-md-1">2</td>
      			  <td class="col-md-1">3</td>
      			  <td class="col-md-1">4</td>
    			</tr>
    		</tbody>
		</table> 
	</div>

		<!-- November -->

	<div class="tab-pane fade" id="november">
		<table class="table table-striped table-hover">
  			<thead>
    			<tr>
	   		 	  <th class="col-md-1">MÅNDAG</th>
    			  <th class="col-md-1">TISDAG</th>
    			  <th class="col-md-1">ONSDAG</th>
    			  <th class="col-md-1">TORSDAG</th>
    			  <th class="col-md-1">FREDAG</th>
    			  <th class="col-md-1">LÖRDAG</th>
    			  <th class="col-md-1">SÖNDAG</th>
   				</tr>
  			</thead>
 			<tbody>
   				<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">1</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">2</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">3</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">4</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">5</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">6</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">7</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">8</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">9</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">10</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">11</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">12</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">13</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">14</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">15</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">16</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">17</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">18</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">19</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">20</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">21</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">22</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">23</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">24</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">25</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">26</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">27</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">28</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">29</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">30</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">31</a></td>
      			  <td class="col-md-1">1</td>
      			  <td class="col-md-1">2</td>
      			  <td class="col-md-1">3</td>
      			  <td class="col-md-1">4</td>
    			</tr>
    		</tbody>
		</table> 
	</div>

		<!-- December -->

	<div class="tab-pane fade" id="december">
		<table class="table table-striped table-hover">
  			<thead>
    			<tr>
	   		 	  <th class="col-md-1">MÅNDAG</th>
    			  <th class="col-md-1">TISDAG</th>
    			  <th class="col-md-1">ONSDAG</th>
    			  <th class="col-md-1">TORSDAG</th>
    			  <th class="col-md-1">FREDAG</th>
    			  <th class="col-md-1">LÖRDAG</th>
    			  <th class="col-md-1">SÖNDAG</th>
   				</tr>
  			</thead>
 			<tbody>
   				<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">1</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">2</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">3</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">4</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">5</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">6</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">7</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">8</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">9</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">10</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">11</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">12</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">13</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">14</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">15</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">16</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">17</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">18</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">19</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">20</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">21</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">22</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">23</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">24</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">25</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">26</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">27</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">28</a></td>
    			</tr>
    			<tr>
      		      <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">29</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">30</a></td>
      			  <td class="col-md-1 day"><a href="#machines" data-toggle="tab" aria-expanded="false" onclick="showMachines()">31</a></td>
      			  <td class="col-md-1">1</td>
      			  <td class="col-md-1">2</td>
      			  <td class="col-md-1">3</td>
      			  <td class="col-md-1">4</td>
    			</tr>
    		</tbody>
		</table> 
	</div>

</div>
</div>


		</div>
        <!-- /.row -->
    </div>

    </div>
    <!-- /.container -->

<!-- Footer -->
        <?php 
        include('footer.php');
        ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>