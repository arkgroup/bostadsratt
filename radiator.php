<?php 
include('head.php');
include('navbar.php');
?>

<body>

	<div class="container">

		<div class="row">

			<div class="col-sm-6 col-sm-offset-3">

				<h1>Element</h1>
				<div class="separator"></div>

				<h4>Varför luftar man?</h4>

				<p>
					Minst en gång per vinter behöver elementen luftas. Om ett element inte är luftat hör man det genom att det bubblar i elementet. Dessutom blir ofta inte hela elementet varmt. Tänk på att dåligt luftade element påverkar samtliga element i systemet. Visa hänsyn till dina grannar och lufta dina element regelbundet. Ju högre upp i huset man bor desto oftare måste man lufta.
				</p>

				<h4>Hur luftar man?</h4>

				<p>
					Högst upp i ena änden på varje element sitter en liten ventil. Den sitter i motsatta änden till temperaturreglaget. Ventilen kan man skruva på med en elementnyckel. Elementnycklar kan man köpa för småpengar i de flesta varuhus samt i fackhandeln. Innan man skruvar på ventilen bör man ha en mugg eller ett kärl tillgängligt. Håll kärlet under ventilen och skruva försiktigt ut ventilen. Behöver elementet luftas så kommer det ut luft eller en blandning av oljigt vatten och luft. Låt ventilen vara öppen tills dess att det bara kommer vatten och stäng den därefter. Elementet är nu luftat.
				</p>
			</div>

		</div>

		<hr>

	</div>

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>