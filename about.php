<?php 
include('head.php');
include('navbar.php');
?>

<body>

    <!-- Page Content -->
    <main class="container">

        <!-- Jumbotron Header -->
        <header>
            <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h1>Allmänt om föreningen</h1>
                <div class="separator"></div>
                <p>
                    Östbo är en privat förening, d v s ingen HSB- eller Riksbyggenförening. Vi är dock medlemmar i SBC och det är även SBC som tillsammans med föreningens kassör ansvarar för löpande bokföring etc. Föreningens nyhetsbrev Ugglan utkommer med viss periodicitet innehållande allt från viktig information till allmänt skvaller.
                    För mer information om Bostadsrättsföreningen Östbo går det bra att vända sig till föreningens ordförande.
                </p>
            </div>
            </div>
        </header>

        <hr>

    </main>

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>
