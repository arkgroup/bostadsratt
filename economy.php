<?php 
include('head.php');
include('navbar.php');
?>

<body>

	<div class="container">

		<div class="row">

			<div class="row">

			<div class="col-sm-6 col-sm-offset-3">

			<h1>Årsredovisningar</h1>
			<div class="separator"></div>

			</div>

			</div>

			<div class="row">

				<div class="col-sm-8 col-sm-offset-2">

			<ul class="pager vertical-align">
  				<div class="col-sm-1">
  				<li class="previous"><a href="#">&larr; Äldre</a></li>
 				</div>

            <div class="col-sm-4">
                <div class="thumbnail" id="info-1">
                    <div class="caption">
                        <h3>2013</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p>
                            <a href="#economy" data-toggle="tab" aria-expanded="true" class="btn btn-info" onclick="activeEconomy()">Ladda ner</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="thumbnail" id="info-2">
                    <div class="caption">
                        <h3>2014</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p>
                            <a href="#board" data-toggle="tab" aria-expanded="true" class="btn btn-info" onclick="activeBoard()">Ladda ner</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="thumbnail" id="info-3">
                    <div class="caption">
                        <h3>2015</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p>
                            <a href="#blueprint" data-toggle="tab" aria-expanded="true" class="btn btn-info" onclick="activeBlueprint()">Ladda ner</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="thumbnail" id="info-4">
                    <div class="caption">
                        <h3>2016</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p>
                            <a href="#resident" data-toggle="tab" aria-expanded="true" class="btn btn-info" onclick="activeResident()">Ladda ner</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-1">
            <li class="next"><a href="#">Nyare &rarr;</a></li>
            </div>
			</ul>

			</div>

			</div>

        </div>

        <hr>

	</div>

<!-- Footer -->
<?php 
include('footer.php');
?>

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Custom -->
<script src="js/custom.js"></script>

</body>