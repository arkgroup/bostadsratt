<?php 
include('head.php');
include('navbar.php');
?>

<body>

	<div class="container">

		<div class="row">

			<div class="col-sm-6 col-sm-offset-3">

				<h1>Journummer</h1>
				<div class="separator"></div>

				<div class="row">

					<div class="col-sm-6">

						<div class="list-group">
  							<a href="#" class="list-group-item">
    						<h4 class="list-group-item-heading">El</h4>
    						<p class="list-group-item-text">
    							Det finns flera säkringsskåp i källaren. <br>
								Det stora finns i elcentralen under A-trappan.
								<br><br>
								Thulins: 046-135 140.
							</p>								
  							</a>
						</div>

						<div class="list-group">
  							<a href="#" class="list-group-item">
    						<h4 class="list-group-item-heading">Glas</h4>
    						<p class="list-group-item-text">
    						Städa så att ingen skadas!
							<br><br>
							Glasmästarn i Lund: 046-211 00 30.
							</p>								
  							</a>
						</div>

						<div class="list-group">
  							<a href="#" class="list-group-item">
    						<h4 class="list-group-item-heading">Lås</h4>
    						<p class="list-group-item-text">
    						Carlbergs jour: 0707-104 104
							</p>								
  							</a>
						</div>

					</div>

					<div class="col-sm-6">

						<div class="list-group">
  							<a href="#" class="list-group-item">
    						<h4 class="list-group-item-heading">Vatten</h4>
    						<p class="list-group-item-text">
    						Stängs av i fjärrvärmecentralen, A-trappan. Instruktioner finns.
							<br><br>
							Rörtjänst i Malmö: 040-22 33 20.
							</p>								
  							</a>
						</div>

						<div class="list-group">
  							<a href="#" class="list-group-item">
    						<h4 class="list-group-item-heading">Fjärrvärme, elementen</h4>
    						<p class="list-group-item-text">
    						Stängs av i fjärrvärmecentralen under A-trappan. Instruktioner finns.
							<br><br>
							Lunds energi: 020-95 19 00.
							</p>								
  							</a>
						</div>

					</div>

				</div>

			</div>

		</div>

		<hr>

	</div>

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>	