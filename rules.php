<?php 
include('head.php');
include('navbar.php');
?>

<body>

	<div class="container">

		<div class="row">

			<div class="col-sm-2 col-sm-offset-1 rule-menu">

				<ul class="nav nav-pills nav-stacked">

					<div class="list-group">

					<li class=""><a href="#" class="list-group-item">Firma och ändamål</a></li>
  					<li class="dropdown">
    				<a class="dropdown-toggle list-group-item" data-toggle="dropdown" href="#">Medlemskap <span class="caret"></span></a>
    					<ul class="dropdown-menu">
      						<li><a href="#">§ 2</a></li>
      						<li><a href="#">§ 3</a></li>
    					</ul>
    				<li class=""><a href="#" class="list-group-item">Insats och avgifter</a></li>
    				<li class=""><a href="#" class="list-group-item">Övergång av bostadsrätt</a></li>
    				<li class="dropdown">
    				<a class="dropdown-toggle list-group-item" data-toggle="dropdown" href="#">Överlåtelseavtalet <span class="caret"></span></a>
    					<ul class="dropdown-menu">
      						<li><a href="#">§ 6</a></li>
      						<li><a href="#">§ 7</a></li>
    					</ul>
    				<li class="dropdown">
    				<a class="dropdown-toggle list-group-item" data-toggle="dropdown" href="#">Rätt till medlemskap vid övergång av bostadsrätt <span class="caret"></span></a>
    					<ul class="dropdown-menu">
      						<li><a href="#">§ 8</a></li>
      						<li><a href="#">§ 9</a></li>
      						<li><a href="#">§ 10</a></li>
    					</ul>
    					
    				</div>

				</ul>

			</div>

			<div class="col-sm-6">

			<h1>Stadgar</h1>
			<div class="separator"></div>
			<p>
			(Stadgarna är även tillgängliga att ladda ner i pdf-format nederst på sidan.)
			</p>

			<h3>Stadgar för Brf Östbo, 745000-0265</h3>

			<h4>Firma och ändamål</h4>

			<p>§ 1</p>

			<p>
			Föreningens firma är bostadsrättsföreningen Östbo och styrelsen har sitt säte i Lund.
			<br><br>
			Föreningen har till ändamål att främja medlemmarnas ekonomiska intressen genom att i föreningens hus upplåta lägenheter åt medlemmarna till nyttjande utan begränsning till tiden. Medlems rätt i föreningen på grund av sådan upplåtelse kallas bostadsrätt. Medlem som innehar bostadsrätt kallas bostadsrättshavare.
			</p>

			<h4>Medlemskap</h4>

			<p>§ 2</p>
			
			<p>
			Medlemskap i föreningen kan beviljas den som erhåller bostadsrätt genom upplåtelse av föreningen eller som övertar bostadsrätt i föreningens hus. Juridisk person som förvärvat bostadsrätt till bostadslägenhet får vägras medlemskap.
			</p>
			
			<p>§ 3</p>

			<p>
			Frågan om att anta en medlem avgörs av styrelsen.
			<br><br>
			Styrelsen är skyldig att snarast, normalt inom en månad från det att skriftlig ansökan om medlemskap kom till föreningen, avgöra frågan om medlemskap.
			</p>

			<h4>Insats och avgifter</h4>

			<p>§ 4</p>

			<p>
			Insats, årsavgift och i förekommande fall upplåtelseavgift fastställs av styrelsen. Ändring av insats skall dock alltid beslutas av föreningsstämman. Årsavgift skall betalas senast sista vardagen före varje kalendermånads början om inte styrelsen beslutat annat.
			<br><br>
			Föreningens löpande kostnader och utgifter samt avsättning till fonder skall finansieras genom att bostadsrättshavarna betalar årsavgift till föreningen. Årsavgiften fördelas på bostadsrättslägenheterna i förhållande till bostadsrätternas andelstal i bostadsrättsföreningen. I årsavgiften ingående konsumtionsavgifter kan beräknas efter förbrukning eller ytenhet.
			<br><br>
			För tillkommande nyttigheter som utnyttjas endast av vissa medlemmar utgår särskild ersättning som bestäms av styrelsen. Om inte avgiften betalas i rätt tid utgår dröjsmålsränta enligt räntelagen på den obetalda avgiften från förfallodagen till dess full betalning sker.
			<br><br>
			Upplåtelseavgift, överlåtelseavgift och pantsättningsavgift får tas ut efter beslut av styrelsen.
			<br><br>
			Överlåtelseavgiften får uppgå till högst 2.5% och pantsättningsavgiften till högst 1% av det basbelopp som gäller vid tidpunkten för ansökan om medlemskap respektive tidpunkten för underrättelse om pantsättning.
			<br><br>
			Överlåtaren av bostadsrätten svarar tillsammans med förvärvaren för att överlåtelseavgiften betalas. Pantsättningsavgift betalas av pantsättaren. Avgiften skall betalas på det sätt styrelsen bestämmer.
			</p>

			<h4>Övergång av bostadsrätt</h4>

			<p>§ 5</p>

			<p>
			Bostadsrättshavaren får fritt överlåta sin bostadsrätt.
			<br><br>
			Bostadsrättshavare som överlåtit sin bostadsrätt till annan medlem skall till bostadsrättsföreningen inlämna en skriftlig anmälan om överlåtelsen med angivande om överlåtelsedag samt till vem överlåtelsen skett.
			<br><br>
			Förvärvare av bostadsrätt skall skriftligen ansöka om medlemskap i bostadsrättsföreningen. I ansökan skall anges personnummer och nuvarande adress.
			<br><br>
			Styrkt kopia av förvärvshandlingen skall alltid bifogas anmälan/ansökan.
			</p>

			<h4>Överlåtelseavtalet</h4>

			<p>§ 6</p>

			<p>
			Ett avtal om överlåtelse av bostadsrätt genom köp skall upprättas skriftligen och skrivas under av säljaren och köparen. I avtalet skall anges den lägenhet som överlåtelsen avser samt köpeskillingen. Motsvarande skall gälla vid byte eller gåva. En överlåtelse som inte uppfyller dessa krav är ogiltig.
			</p>
			
			<p>§ 7</p>

			<p>
			När en bostadsrätt överlåtits till en ny innehavare, får denne utöva bostadsrätten och tillträda lägenheten endast om han har antagits som medlem i föreningen.
			<br><br>
			Om förvärvaren i strid med §7 1st utövar bostadsrätten och tillträder lägenheten innan medlemskap beviljats har föreningen rätt att vägra medlemskap. Ett dödsbo efter avliden bostadsrättshavare får utöva bostadsrätten trots att dödsboet inte är medlem i föreningen.
			<br><br>
			Efter tre år från dödsfallet får föreningen dock anmana dödsboet att inom sex månader från anmaning visa att bostadsrätten ingått i bodelning eller arvskifte med anledning av bostadsrättshavarens död eller att någon, som inte får vägras inträde i föreningen, förvärvat bostadsrätten och sökt medlemskap. Om den tid som angivits i anmaningen inte iakttas, får bostadsrätten säljas på offentlig auktion för dödsboets räkning.
			</p>

			<h4>Rätt till medlemskap vid övergång av bostadsrätt</h4>

			<p>§ 8</p>

			<p>
			Den som en bostadsrätt har övergått till får inte vägras medlemskap i föreningen om föreningen skäligen gör godta förvärvaren som bostadsrättshavare. Om det kan antas att förvärvaren för egen del inte permanent skall bosätta sig i bostadsrättslägenheten har föreningen rätt att vägra medlemskap.
			<br><br>
			En juridisk person samt fysisk omyndig person som har förvärvat bostadsrätt till en bostadslägenhet får vägras medlemskap i föreningen.
			<br><br>
			Har en bostadsrätt övergått till make eller sambo på vilka lagen om sambor gemensamma hem skall tillämpas får medlemskap vägras om förvärvaren inte kommer att vara permanent bosatt i lägenheten efter förvärvet.
			<br><br>
			Den som har förvärvat andel i bostadsrätt får vägras medlemskap i föreningen om inte bostadsrätten efter förvärvet innehas av makar eller sådana sambor på vilka lagen om sambors gemensamma hem skall tillämpas.
			</p>
			
			<p>§ 9</p>

			<p>
			Om en bostadsrätt övergått genom bodelning, arv, testamente, bolagsskifte eller liknande förvärv och förvärvaren inte antagits till medlem, får föreningen anmana förvärvaren att inom sex månader från anmaningen visa att någon, som inte får vägras inträde i föreningen, förvärvat bostadsrätten och sökt medlemskap. Iakttas inte den tid som angivits i anmaningen, får bostadsrätten säljas på offentlig auktion för förvärvarens räkning.
			</p>

			<p>§ 10</p>

			<p>
			En överlåtelse är ogiltig om den som bostadsrätten överlåtits till vägras medlemskap.
			<br><br>
			Första stycket gäller inte vid exekutiv försäljning eller offentlig auktion enligt bestämmelserna i bostadsrättslagen. Har i sådant fall förvärvaren inte antagits medlem, skall föreningen lösa bostadsrätten mot skälig ersättning.
			</p>

			<h4>Bostadsrättshavarens rättigheter och skyldigheter</h4>

			<p>§ 11</p>

			<p>
			Bostadsrättshavaren skall på egen bekostnad hålla lägenhetens inre med tillhörande övriga utrymmen i gott skick. Föreningen svarar för husets skick i övrigt.
			<br><br>
			Bostadsrättshavaren svarar sålunda för lägenhetens
			-	väggar, golv och tak samt underliggande fuktisolerande skikt;
			-	inredning och utrustning - såsom ledningar och övriga installationer för vatten, avlopp, värme, gas, ventilation och el till de delar dessa befinner sig inne i lägenheten och inte är stamledningar;
			-	svagströmsanläggningar;
			-	i fråga om vattenfyllda radiatorer och stamledningar svarar bostadsrättshavaren dock endast för målning;
			-	i fråga om stamledningar för el svarar bostadsrättshavaren endast fr o m lägenhetens undercentral;
			-	golvbrunnar, ventilationskanaler, inner- och ytterdörrar samt glas och bågar i fönster och fönsterdörrar.
			-	Om inte föreningen beslutar annat i samband med att den genomför väsentliga underhållsarbeten.
			<br><br>
			Bostadsrättshavaren svarar dock inte för målning av yttersidorna av ytterdörrar och ytterfönster.
			<br><br>
			Bostadsrättshavaren svarar för reparation i anledning av brand eller vattenledningsskada i lägenheten endast om skadan har uppkommit genom bostadsrättshavarens eget vållande eller genom vårdslöshet eller försummelse av någon som tillhör hans hushåll eller gästar honom eller av annan som han inrymt i lägenheten eller som utfört arbete för hans räkning.
			<br><br>
			I fråga om brandskada som bostadsrättshavaren inte själv vållat gäller vad som nu sagts endast om bostadsrättshavaren brustit i den omsorg och tillsyn som han bort iaktta.
			<br><br>
			Om ohyra förekommer i lägenheten skall motsvarande ansvarsfördelning gälla som vid brand eller vattenledningsskada.
			<br><br>
			Är bostadsrätten försedd med balkong, terrass, uteplats eller med egen ingång, skall bostadsrättshavaren svara för renhållning och snöskottning.
			<br><br>
			Bostadsrättshavaren svarar för åtgärder i lägenheten som vidtagits av tidigare bostadsrättshavare såsom reparationer, underhåll, installationer mm. Om bostadsrättshavaren anser att föreningen skall bekosta eventuell reparation skall styrelsen kontaktas innan reparatör tillkallas.
			<br><br>
			Med bostadsrätten följer nyttjanderätt till de antal förråd vilka ingick det datum skriftligt avtal om överlåtelse av bostadsrätt tecknades. Bostadsrättshavaren kan avsäga sig nyttjanderätten till förmån för föreningen. Detta skall ske skriftligen. Bostadsrättshavaren kan byta nyttjanderätt till förråd med annan bostadsrättshavare. Sådant byte skall ansökas skriftligen om hos styrelsen som vid godkännande skall underteckna ansökan.
			</p>
			
			<p>§ 12</p>

			<p>
			Bostadsrättshavaren får inte göra någon väsentlig förändring av lägenheten utan tillstånd av styrelsen.
			<br><br>
			En förändring får aldrig innebära bestående olägenheter för föreningen eller annan medlem. Underhålls- och reparationsåtgärder skall utföras på ett fackmannamässigt sätt enligt gällande byggnorm. Eventuella skador eller extra utgifter orsakade av om- och tillbyggnad av bostadsrätt står innehavaren för.
			<br><br>
			Som väsentlig förändring räknas bl a alltid förändring som kräver bygglov eller innebär ändring av ledning för vatten, avlopp eller värme.
			<br><br>
			Bostadsrättshavaren svarar för att erforderliga myndighetstillstånd erhålls.
			</p>
			
			<p>§ 13</p>

			<p>
			Bostadsrättshavaren är skyldig att när han använder lägenheten och andra delar av fastigheten iaktta allt som fordras för att bevara sundhet, ordning och skick inom fastigheten. Han skall rätta sig efter de särskilda regler som föreningen meddelar i överensstämmelse med ortens sed. Bostadsrättshavaren skall hålla noggrann tillsyn över att detta också iakttas av den som hör till hans hushåll eller gästar honom eller av någon annan som han inrymt i lägenheten eller som dör utför arbete för hans räkning.
			<br><br>
			Föremål som enligt vad bostadsrättshavaren vet är eller med skäl kan misttänkas vara behäftat med ohyra får inte föras in i lägenheten.
			</p>

			<p>§ 14</p>

			<p>
			Företrädare för bostadsrättsföreningen har rätt att få komma in i lägenheten när det behövs för tillsyn eller för att utföra arbete som föreningen svarar för. Skriftligt meddelande om detta skall läggas i lägenhetens brevlådeinkast eller anslås i trappuppgången.
			<br><br>
			Bostadsrättshavaren är skyldig att tåla sådana inskränkningar i nyttjanderätten som föranleds av nödvändiga åtgärder för att utrota ohyra i fastigheten, även om hans lägenhet inte besväras av ohyra.
			<br><br>
			Om bostadsrättshavaren inte lämnar föreningen tillträde till lägenheten när föreningen har rätt till det, får beslutas om handräckning.
			</p>

			<p>§ 15</p>

			<p>
			En bostadsrättshavare får upplåta hela sin lägenhet i andra hand endast om styrelsen ger sitt samtycke.
			</p>

			<p>§ 16</p>

			<p>
			Bostadsrättshavaren får inte inrymma utomstående personer i lägenheten, om det kan medföra men för föreningen eller någon annan medlem i föreningen.
			</p>

			<p>§ 17</p>

			<p>
			Bostadsrättshavaren får inte använda lägenheten för något annat ändamål än det avsedda. Föreningen får dock endast åberopa avvikelse som är av avsevärd betydelse för föreningen eller någon annan medlem i föreningen.
			</p>
			
			<p>§ 18</p>

			<p>
			Om bostadsrättshavaren inte i rätt tid betalar insats eller upplåtelseavgift som skall betalas innan lägenheten får tillträdas och sker inte heller rättelsen inom en månad från anmaning får föreningen häva upplåtelseavtalet. Detta lägger inte om lägenheten tillträtts med styrelsens medgivande.
			<br><br>
			Om avtalet hävs har föreningen rätt till skadestånd.
			</p>

			<p>§ 19</p>

			<p>
			Nyttjanderätten till en lägenhet som innehas med bostadsrätt och som tillträtts är med de begränsningar som följer nedan förverkad och föreningen således berättigad att säga upp bostadsrättshavaren till avflyttning,
			</p>	
				<ol>
					<li>om bostadsrättshavaren dröjer med att betala insats eller upplåtelseavgift utöver två veckor från det att föreningen efter förfallodagen anmanat honom att fullgöra sin betalningsskyldighet eller om bostadsrättshavaren dröjer med att betala årsavgift två vardagar från förfallodagen,</li>
					<li>om bostadsrättshavaren utan behövligt samtycke eller tillstånd upplåter lägenheten i andra hand,</li>
					<li>om lägenheten används i strid med 16:e eller 17:e §.</li>
					<li>om bostadsrättshavaren eller den, som lägenheten upplåtits till i andrahand, genom vårdslöshet är vållande till att det finns ohyra i lägenheten eller om bostadsrättshavaren genom att inte utan oskäligt dröjsmål underättat styrelsen om att det finns ohyra i fastigheten bidrar till att ohyran sprids i fastigheten,</li>
					<li>om lägenheten på annat sätt vanvårdas eller om bostadsrättshavaren eller den, som lägenheten upplåtits till i andrahand, åsidosätter något av vad som skall iakttas enligt 13 § vid lägenhetens begagnande eller brister i den tillsyn som enligt samma paragraf åligger en bostadsrättshavare,</li>
					<li>om bostadsrättshavaren inte lämnar tillträde till lägenheten enligt 14 § och inte kan visa giltig ursäkt för detta,</li>
					<li>om inte bostadsrättshavaren inte fullgör annan skyldighet och det måste anses vara av synnerlig vikt för föreningen att skyldigheten fullgörs, samt</li>
					<li>om lägenheten helt eller till väsentlig del används för näringsverksamhet eller därmed likartad verksamhet, vilken utgör eller i vilken till en inte oväsentlig del ingår brottsligt förfarande eller för tillfälliga sexuella förbindelser mot ersättning.</li>
				</ol>
			<p>
			Nyttjanderätten är inte förverkad om det som ligger bostadsrättshavaren till last är av ringa betydelse.
			</p>

			<p>§ 20</p>

			<p>
			Uppsägning som avses i 19 § första stycket 2, 3 eller 5-7 får sek endast om bostadsrättshavaren låter bli att efter tillsägelse vidta rättelse utan dröjsmål.
			<br><br>
			I fråga om en bostadsrättslägenhet får uppsägning på grund av förhållanden som avses i 19 § första stycket 2 inte heller ske om bostadsrättshavaren efter tillsägelse utan dröjsmål ansöker om tillstånd till upplåtelsen och får ansökan beviljad.
			</p>
		
			<p>§ 21</p>

			<p>
			Är nyttjanderätten förverkad på grund av förhållande som avses i 19 § första stycket 1-3 eller 5-7 men sker rättelse innan föreningen gjort bruk av sin rätt till uppsägning, kan bostadsrättshavaren inte därefter skiljas från lägenheten på den grunden. Detsamma gäller om föreningen inte har sagt upp bostadsrättshavaren till avflyttning inom tre månader från den dag föreningen fick reda på förhållanden som avses i 19 § första stycket 4 eller 7 eller inte inom två månader från den dag då föreningen fick reda på förhållande som avses i 19 § första stycket 2 sagt till bostadsrättshavaren att vidta rättelse.
			</p>

			<p>§ 22</p>

			<p>
			Är nyttjanderätten enligt 19 § första stycket 1 förverkad på grund av dröjsmål med betalning av årsavgift, och har föreningen med anledning av detta sagt upp bostadsrättshavaren till avflyttning, får denne på grund av dröjsmålet inte skiljas från lägenheten om avgiften betalas senast tolfte vardagen från uppsägningen.
			<br><br>
			I väntan på att bostadsrättshavaren visar sig ha fullgjort vad som fordras för att få tillbaka nyttjanderätten får beslut om avhysning inte meddelas förrän efter fjorton vardagar från den dag bostadsrättshavaren sades upp.
			</p>
			
			<p>§ 23</p>

			<p>
			Sägs bostadsrättshavaren upp till avflyttning av någon orsak som anges i 19 § första stycket 1, 4-6 eller 8 är han skyldig att flytta genast, om inte annat följer av 22 §. Sägs bostadsrättshavaren upp av någon annan i 19 § första stycket angiven orsak, får han bo kvar till det månadsskifte som inträffar närmast efter tre månader från uppsägningen, om inte rätten ålägger honom att flytta tidigare.
			</p>
			
			<p>§ 24</p>

			<p>
			Om föreningen säger upp bostadsrättshavaren till avflyttning, har föreningen rätt till skadestånd.
			</p>

			<p>§ 25</p>

			<p>
			Har bostadsrättshavaren blivit skild från lägenheten till följd av uppsägning i fall som avses i 19 §, skall bostadsrätten säljas på offentlig auktion så snart det kan ske om inte föreningen, bostadsrättshavaren och de kända borgenärer vars rätt berörs av försäljningen kommer överens om något annat. Försäljningen får dock anstå till dess att brister som bostadsrättshavaren svarar för blivit åtgärdade.
			</p>

			<h4>Styrelse och revisorer</h4>

			<p>§ 26</p>

			<p>
			Styrelsen svarar för föreningens organisation och förvaltning av föreningens angelägenheter.
			</p>
			
			<p>§ 27</p>

			<p>
			Styrelsen består av minst fem och högst sju ledamöter med minst en och högst tre suppleanter.
			<br><br>
			Styrelseledamöter och suppleanter väljs av föreningsstämman för två år. Ledamot och suppleant kan omväljas. Av styrelsens ledamöter skall alltid minst tre eller, om styrelsen består av mindre än sju ledamöter, minst två väljas på ordinarie föreningsstämma.
			<br><br>
			Till styrelseledamot och suppleant kan förutom medlem väljas även make till medlem och närstående som varaktigt sammanbor med medlemmen. Valbar är endast myndig person som är bosatt i föreningens fastighet.
			</p>

			<p>§ 28</p>

			<p>
			Styrelseordförande samt kassör väljs på stämma. I övrigt konstituerar sig styrelsen själv.
			<br><br>
			Styrelsen är beslutsför när antalet närvarande ledamöter vid sammanträdet överstiger hälften av samtliga styrelseledamöter. Som styrelsens beslut gäller den mening för vilken mer än hälften av de närvarande röstat eller vid lika röstetal den mening som biträds av ordföranden, dock fordras för giltigt beslut enhällighet när för beslutsförhet minsta antalet ledamöter är närvarande.
			<br><br>
			Föreningens firma tecknas av två styrelseledamöter i förening.
			</p>
			
			<p>§ 29</p>

			<p>
			Föreningens räkenskapsår omfattar tiden 1 januari - 31 december. Före mars månads utgång varje år skall styrelsen till revisorerna avlämna förvaltningsberättelse, resultaträkning samt balansräkning.
			</p>
			
			<p>§ 30</p>

			<p>
			Styrelsen eller firmatecknare får inte utan föreningsstämmans bemyndigande avhända föreningen dess fasta egendom eller tomträtt och inte heller riva eller företa mer omfattande till- eller ombyggnadsåtgärder av sådan egendom.
			</p>

			<p>§ 31</p>

			<p>
			Revisorerna ska vara en till tre vilket också gäller suppleanterna. Revisorer och revisorssuppleanter väljs på föreningsstämma för tiden från ordinarie föreningsstämma fram t o m nästa ordinarie stämma.
			</p>

			<p>§ 32</p>

			<p>
			Revisorerna skall bedriva sitt arbete så att revision är avslutad och revisionsberättelsen avgiven senast den 15 april. Styrelsen skall avge skriftlig förklaring till ordinarie föreningsstämma över av revisorerna gjorda anmärkningar. Styrelsens revisionshandlingar, revisionsberättelsen och styrelsens förklaring över av revisorerna gjorda anmärkningar skall hållas tillgängliga medlemmarna minst en vecka före den föreningsstämma på vilken ärendet skall förekomma till behandling.
			</p>

			<h4>Föreningsstämma</h4>

			<p>§ 33</p>

			<p>
			Ordinarie föreningsstämma skall hållas årligen före maj månads utgång. För att visst ärende som medlem önskar får behandlat på föreningsstämma skall kunna anges i kallelsen till denna skall ärendet skriftligen anmälas till styrelsen före april månads utgång eller den senare tidpunkt som styrelsen kan komma att bestämma.
			</p>

			<p>§ 34</p>

			<p>
			Extra föreningsstämma skall hållas när styrelsen eller revisor finner skäl till det eller när minst 1/10 av samtliga röstberättigade skriftligen begär det hos styrelsen med angivande av ärende som önskas behandlat på stämman.
			</p>

			<p>§ 35</p>

			<p>
			På ordinarie föreningsstämma skall förekomma:
			</p>
				<ol>
					<li>Stämmans öppnande</li>
					<li>Godkännande av dagordningen</li>
					<li>Val av stämmoordförande</li>
					<li>Anmälan av stämmoordförandes val av protokollförare.</li>
					<li>Val av två justeringsmän tillika rösträknare.</li>
					<li>Frågan om stämman blivit i stadgeenlig ordning utlyst.</li>
					<li>Fastställande av röstlängd</li>
					<li>Föredragning av styrelsens årsredovisning.</li>
					<li>Föredragning av revisorns berättelse.</li>
					<li>Beslut om fastställande av resultat- och balansräkning.</li>
					<li>Beslut om resultatdisposition</li>
					<li>Beslut om ansvarsfrihet för styrelseledamöterna.</li>
					<li>Beslut om arvoden åt styrelseledamöter och revisorer för nästkommande verksamhetsår.</li>
					<li>Val av styrelseledamöter och suppleanter.</li>
					<li>Val av revisorer och revisorssuppleanter</li>
					<li>Val av valberedning.</li>
					<li>Av styrelsen till stämman hänskjutna frågor samt av föreningsmedlem anmält ärende enligt § 33.</li>
					<li>Stämmans avslutande.</li>
				</ol>
			<p>
			<br>
			På extra föreningsstämma skall utöver ärenden enligt 1-7 ovan endast förekomma de ärenden för vilka stämman blivit utlyst och vilka angivits i kallelsen till stämman.
			</p>

			<p>§ 36</p>

			<p>
			Kallelse till föreningsstämma skall innehålla uppgift om vilka ärenden som skall behandlas på stämman. Även ärende som anmälts av styrelsen eller av föreningsmedlem enligt § 33 skall anges i kallelsen. Denna skall utfärdas genom personlig kallelse till samtliga medlemmar genom utdelning eller genom postbefordran senast två veckor för ordinarie och en vecka före extra föreningsstämma, dock tidigast fyra veckor före stämman.
			<br><br>
			Andra meddelanden till medlemmarna anslås på lämplig plats inom föreningens fastighet eller genom utdelning eller postbefordran av brev.
			</p>

			<p>§ 37</p>

			<p>
			Vid föreningsstämman har varje medlem en röst. Om flera medlemmar innehar bostadsrätt gemensamt har de dock gemensamt endast en röst vilket också gäller om de har flera bostadsrätter.
			<br><br>
			Medlem får utöva sin rösträtt genom ombud. Ombudet skall förete en skriftlig, dagtecknad fullmakt, ej äldre än ett år. Endast annan medlem, make eller närstående, som varaktigt sammanbor med medlemmen får vara ombud. Ingen får såsom ombud företräda mer än en medlem.
			<br><br>
			Röstberättigad är endast den medlem som fullgjort sina åtaganden mot föreningen enligt dessa stadgar eller enligt lag.
			</p>

			<p>§ 38</p>

			<p>
			Det justerade protokollet från föreningsstämman skall hållas tillgängligt för medlemmarna senast tre veckor efter stämman.
			</p>
			
			<p>§ 39</p>

			<p>
			Inom föreningen skall en fond för yttre underhåll finnas och det bör finnas en dispositionsfond.
			<br><br>
			Till yttre underhållsfonden skall årligen avsättas minst ett belopp på 1.0% av fastighetens taxeringsvärde.
			<br><br>
			Det överskott som kan uppstå på föreningens verksamhet skall avsättas till dispositionsfond eller disponeras på annat sätt i enlighet med föreningsstämmans beslut.
			</p>

			<h4>Vinst</h4>

			<p>§ 40</p>

			<p>
			Om föreningsstämman beslutar att uppkommen vinst skall delas ut skall vinsten fördelas mellan medlemmarna i förhållande till andelstal i bostadsrättsföreningen.
			</p>
			
			<p>§ 41</p>

			<p>
			Vid föreningens upplösning skall behållna tillgångar tillfalla medlemmarna i förhållande till andelstal i bostadsrättsföreningen.
			</p>

			<h4>Övrigt</h4>

			<p>
			Bostadsrättshavare är skyldig att alltid ha en gällande hemförsäkring. Ett s k bostadsrättstillägg skall finnas i försäkringen om ej detta tillägg ingår i föreningens fastighetsförsäkring.
			<br><br>
			Bostadsrättshavare har skyldighet att delta i gemensamma arbetsuppgifter som stämman beslutar om.
			<br><br>
			För frågor som inte regleras i dessa stadgar gäller bostadsrättslagen, lagen om ekonomiska föreningar samt övrig lagstiftning.
			</p>

			</div>

		</div>

		<hr>

	</div>

	<!-- Footer -->
	<?php 
	include('footer.php');
	?>

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

	<!-- Custom -->
	<script src="js/custom.js"></script>

</body>