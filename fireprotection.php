<?php 
include('head.php');
include('navbar.php');
?>

<body>

	<div class="container">

		<div class="row">

			<div class="col-sm-6 col-sm-offset-3">

				<h1>Brandskydd</h1>
				<div class="separator"></div>

				<p>
					Varje lägenhet skall vara utrustad med minst en fungerande brandvarnare. Både föreningens regler samt brandskyddsmyndigheten kräver detta. Men det är inte därför du ska ha en. Du ska ha den för att den kan rädda ditt och dina grannars liv.
					<br><br>
					I varje trappuppgång finns två skumsläckare. En vid utgången mot gården och en bredvid dörren till vinden. Det finns även en skumsläckare i källaren, i relaxrummet innan bastun. Dessa kontrolleras regelbundet av certifierad personal så att de alltid ska vara funktionella. Att skaffa sig en egen brandsläckare att ha i lägenheten är inget krav - men ett väldigt bra tips.
					<br><br>
					I trappan på varje våningsplan finns en brandvarnare. De sitter ovanpå lamporna i taket. Batterierna ska kollas regelbundet, lämpligen i samband med fixardagarna.
				</p>
				
			</div>

		</div>

		<hr>

	</div>

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>