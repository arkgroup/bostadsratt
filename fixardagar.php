<?php 
include('head.php');
include('navbar.php');
?>

<body>

	<div class="container">

		<div class="row">

			<div class="col-sm-6 col-sm-offset-3">

				<h1>Fixardagar</h1>
				<div class="separator"></div>

				<p>
					En helgdag varje höst och vår har vi en så kallad fixardag. Datum för detta brukar aviseras i god tid på anslagstavlan. Då passar vi på att gemensamt ta hand om saker som behöver åtgärdas i trädgård eller gemensamma utrymmen i huset. Det kan röra sig om att plantera nya växter, rensa ogräs, olja utemöbler, köra bort gamla cyklar som ingen vill kännas vid, måla en vägg etc. Vi brukar ha riktigt trevligt, fastigheten blir underhållen och du lär känna dina grannar.
					<br><br>
					Efter uträttat arbete brukar vi äta en god gemensam lunch som föreningen bjuder på.
					<br><br>
					Kan du inte vara med ombedes du kontakta styrelsen så att du istället kan få ett beting. Det finns alltid saker som behöver åtgärdas.
				</p>

			</div>

		</div>

		<hr>

	</div>

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>	