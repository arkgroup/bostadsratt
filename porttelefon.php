<?php 
include('head.php');
include('navbar.php');
?>

<body>

	<div class="container">

		<div class="row">

			<div class="col-sm-6 col-sm-offset-3">

				<h1>Porttelefon</h1>
				<div class="separator"></div>

					<h4>Porttelefon</h4>

						<p>
						Ytterdörrarna mot Bankgatan är utrustade med porttelefoner. Porttelefonerna är kopplade till det vanliga telefonnätet. När någon ringer upp till en lägenhet via porttelefonen ringer det i lägenhetens vanliga telefoner. Man känner igen ett samtal från porttelefonen på att en liten melodislinga spelas i luren innan man kan börja prata med personen på gatan. Efter 30 sekunder spelas samma slinga igen och samtalet bryts.
						</p>

					<h4>Öppning av port</h4>

						<p>
						För att kunna öppna dörren krävs att man svarat i en tonvalstelefon. Dörren öppnas då man trycker på knappen ‘5′.
						</p>

					<h4>Avvisning vid porten</h4>

						<p>
						Ringer oönskad/okänd gäst på dörren, lägg på luren så hålls dörren låst.
						</p>

					<h4>Hur når man en lägenhet?</h4>

						<p>
						Varje lägenhet kan nås på två sätt. Ute i porten finns en tavla som anger en kod till varje lägenhet, t.ex. 0004. Vill man ringa till lägenheten som svarar mot 0004 så trycker man ‘B0004′ på porttelefonen. Detta fungerar bara mellan klockan 7.00 och 21.00. Varje lägenhet kan dessutom nås genom att man slår ‘B’ följt av lägenhetens telefonnummer. Detta alternativ fungerar dygnet runt.
						</p>

					<h4>Upptaget</h4>

						<p>
						Om man ringer en lägenhet från porttelefonen och det är upptaget så kan man direkt ringa lägenheten igen. Då öppnas porten (om det fortfarande är upptaget) utan att man fått kontakt med någon i lägenheten.
						</p>
						
			</div>

		</div>

		<hr>

	</div>

    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>	