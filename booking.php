<?php 
include('head.php');
include('navbar-logged-in.php');
?>

<body onload="thisMonth(), thisDay()">

    <div class="container">

      <div class="row text-center">

        <div class="col-sm-6 col-sm-offset-3">

          <h1>Tvättbokning</h1>
          <div class="separator"></div>

        </div>

      </div>
    <!-- Page Content -->
   <div class="booking-table">
        <ul class="nav nav-tabs center">
            <li class="active">
                <a href="#available" data-toggle="tab" aria-expanded="true" onclick="document.getElementById('days').className = 'tab-content', thisMonth()">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3>Tillgängliga tider</h3>
                        </div>
                    </div>
                </a>
            </li>
            <li class="">
                <a href="#reserved" data-toggle="tab" aria-expanded="true" onclick="document.getElementById('days').className += ' hide', document.getElementById('machines').className += ' hide', resetMonth()">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3>Dina bokade tider</h3>
                        </div>
                    </div>
                </a>
            </li>
        </ul>
       
        <!-- /.row -->

        <!-- Page Content -->
        <div class="row">
<div class="col-sm-12">

    <!-- Months -->

<div id="months" class="tab-content">

        <!-- Available -->

  <div class="tab-pane fade active in" id="available">
    <ul class="nav nav-tabs">
        <li class="" id="jan-0"><a href="#january" data-toggle="tab" aria-expanded="true">Januari</a></li>
        <li class="" id="feb-1"><a href="#february" data-toggle="tab" aria-expanded="false">Februari</a></li>
        <li class="" id="mar-2"><a href="#mars" data-toggle="tab" aria-expanded="false">Mars</a></li>
        <li class="" id="apr-3"><a href="#april" data-toggle="tab" aria-expanded="false">April</a></li>
        <li class="" id="may-4"><a href="#may" data-toggle="tab" aria-expanded="false">Maj</a></li>
        <li class="" id="jun-5"><a href="#june" data-toggle="tab" aria-expanded="false">Juni</a></li>
        <li class="" id="jul-6"><a href="#july" data-toggle="tab" aria-expanded="false">Juli</a></li>
        <li class="" id="aug-7"><a href="#august" data-toggle="tab" aria-expanded="false">Augusti</a></li>
        <li class="" id="sep-8"><a href="#september" data-toggle="tab" aria-expanded="false">September</a></li>
        <li class="" id="oct-9"><a href="#october" data-toggle="tab" aria-expanded="false">Oktober</a></li>
        <li class="" id="nov-10"><a href="#november" data-toggle="tab" aria-expanded="false">November</a></li>
        <li class="" id="dec-11"><a href="#december" data-toggle="tab" aria-expanded="false">December</a></li>
    </ul>
  </div>

        <!-- Reserved -->

  <div class="tab-pane fade" id="reserved">
    <h1>Inga bokade tider</h1>
  </div>
</div>

<script>

        // labels for weekdays
        cal_days_labels = ['Måndag', 'Tisdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lördag', 'Söndag'];

        // labels for months
        cal_months_labels = ['january', 'february', 'mars', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];

        // amount of days per month
        cal_days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // current date
        cal_current_date = new Date(); 

        function Calendar(month, year) {
            this.month = (isNaN(month) || month == null) ? cal_current_date.getMonth() : month;
            this.year  = (isNaN(year) || year == null) ? cal_current_date.getFullYear() : year;
            this.html = '';
        }



Calendar.prototype.generateHTML = function(){

  // get first day of month
  var firstDay = new Date(this.year, this.month, 1);
  var startingDay = firstDay.getDay();
  
  // find number of days in month
  var monthLength = cal_days_in_month[this.month];
  
  // do the header
  // useless var monthName = cal_months_labels[this.month]
  

  // create table
  var html = '<div id="days" class="tab-content">'

  //create month tabs
  for(var k = 0; k < 12; k++){
    monthLength = cal_days_in_month[k];
    firstDay = new Date(this.year, k, 0);
    startingDay = firstDay.getDay();
    var l = cal_months_labels[k];

    //compensate for leap year
  if (k == 1) { // February only!
    if((this.year % 4 == 0 && this.year % 100 != 0) || this.year % 400 == 0){
      monthLength = 29;
    }
  }
   
    html += '<div class="tab-pane fade" id="'+l+'">';
    html += '<table class="table table-striped table-hover">';
    html += '<thead><tr>';

    //create weekday header
  for(var i = 0; i <= 6; i++ ){
    html += '<th class="col-md-1">';
    html += cal_days_labels[i];
    html += '</td>';
  }

    html += '</tr></thead><tbody><tr>';

  // fill in the days
  var day = 1;
  // this loop is for is weeks (rows)
  for (var i = 0; i < 9; i++) {
    // this loop is for weekdays (cells)
    for (var j = 0; j <= 6; j++) { 
      html += '<td class="col-md-1 day" id="'+l+'-'+day+'">';
      if (day <= monthLength && (i > 0 || j >= startingDay)) {
        html += '<a href="#machines" data-toggle="tab" aria-expanded="false" value="'+day+'" onclick="showMachines()">'+day+'</a>';
        day++;
      }
      else {
      html += '-</td>';
      }
      html += '</td>';
    }
    // stop making rows if we've run out of days
    if (day > monthLength) {
      break;
    } else {
      html += '</tr><tr>';
    }
  }
  html += '</tr></tbody></table></div>';
  }

  html += '</div>'
  html += '     <div class="tab-pane fade hide" id="machines">'
  html += '         <div class="list-group">'
  html += '                  <h3>Tillgängliga tvättmaskiner</h3>'
  html += '                  <h4>'+day+'</h4>'
  html += '              <div class="machine-wrapper col-sm-12 list-group-item">'
  html += '                  <h4 class="list-group-item-heading">8.00-12.00 - Tvättmaskin 1'
  html += '                  <a href="#" class="btn btn-success btn-lg book-btn">Boka</a></h4>'
  html += '              </div>'
  html += '              <div class="machine-wrapper col-sm-12 list-group-item">'
  html += '                  <h4 class="list-group-item-heading">8.00-12.00 - Tvättmaskin 2'
  html += '                  <a href="#" class="btn btn-success btn-lg book-btn">Boka</a></h4>'
  html += '              </div>'
  html += '              <div class="machine-wrapper col-sm-12 list-group-item">'
  html += '                  <h4 class="list-group-item-heading">8.00-12.00 - Tvättmaskin 3'
  html += '                  <a href="#" class="btn btn-success btn-lg book-btn">Boka</a></h4>'
  html += '              </div>'
  html += '              <div class="machine-wrapper col-sm-12 list-group-item">'
  html += '                  <h4 class="list-group-item-heading">12.00-16.00 - Tvättmaskin 1'
  html += '                  <a href="#" class="btn btn-success btn-lg book-btn">Boka</a></h4>'
  html += '              </div>'
  html += '              <div class="machine-wrapper col-sm-12 list-group-item">'
  html += '                  <h4 class="list-group-item-heading">12.00-16.00 - Tvättmaskin 2'
  html += '                  <a href="#" class="btn btn-success btn-lg book-btn">Boka</a></h4>'
  html += '              </div>'
  html += '              <div class="machine-wrapper col-sm-12 list-group-item">'
  html += '                  <h4 class="list-group-item-heading">12.00-16.00 - Tvättmaskin 3'
  html += '                  <a href="#" class="btn btn-success btn-lg book-btn">Boka</a></h4>'
  html += '              </div>'
  html += '              <div class="machine-wrapper col-sm-12 list-group-item unavailable">'
  html += '                  <h4 class="list-group-item-heading">16.00-20.00 - Tvättmaskin 1'
  html += '                  <a href="#" class="btn btn-danger btn-lg book-btn disabled">Boka</a></h4>'
  html += '              </div>'
  html += '              <div class="machine-wrapper col-sm-12 list-group-item unavailable">'
  html += '                  <h4 class="list-group-item-heading">16.00-20.00 - Tvättmaskin 2'
  html += '                  <a href="#" class="btn btn-danger btn-lg book-btn disabled">Boka</a></h4>'
  html += '              </div>'
  html += '              <div class="machine-wrapper col-sm-12 list-group-item unavailable">'
  html += '                  <h4 class="list-group-item-heading">16.00-20.00 - Tvättmaskin 3'
  html += '                  <a href="#" class="btn btn-danger btn-lg book-btn disabled">Boka</a></h4>'
  html += '              </div>'
  html += '          </div>'
  html += '      </div>'
  


  this.html = html;
}

Calendar.prototype.getHTML = function() {
  return this.html;
}

</script>

<script>
  var cal = new Calendar();
  cal.generateHTML();
  document.write(cal.getHTML());
</script>

    </div>
    <!-- /.container -->

</div>
</div>
</div>
    <!-- Footer -->
    <?php 
    include('footer.php');
    ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom -->
    <script src="js/custom.js"></script>

</body>